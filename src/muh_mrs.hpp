// Say Hi to the MRS Anon,
// Muh Robowaifu Simulator
//========================
// The Young Wife Series:
// Chii-chan Edition
//

// filename: muh_mrs.hpp
// This file is the central core 'switchboard' of the MRS.

#pragma once

#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <locale>
#include <string>

// GLAD
#include "glad.h"  // holds all OpenGL type declarations (#include before glfw)

// GLFW
#include <GLFW/glfw3.h>

// GLM
#include <glm/glm.hpp>

//
#include "Camera.h"

// Also part of the mrs namespace
#include "muh_gym.hpp"
#include "muh_help.hpp"
#include "muh_inputs.hpp"
#include "muh_text.hpp"
#include "muh_toys.hpp"
//
#include "muh_globals.hpp"
namespace toys = mrs::toys;
namespace text = mrs::text;
namespace help = mrs::help;
namespace msys = mrs::mrssys;
namespace mtime = mrs::timing;
namespace mouse = mrs::input::mouse;
namespace key = mrs::input::key;

using msys::mwin;

using std::cerr;
using std::cout;
using std::exit;
using std::string;
using std::to_string;

#define NDEBUG 1

namespace mrs {

//------------------------------------------------------------------------------

// Forward decls
//--------------
void init_system();
void frmbuff_rsz_cb(GLFWwindow*, int, int);  // system callback

// Initialize the GLFW window and OpenGL for use
// -windowed (by default), or selectable fullscreen modes supported.
void init_win(const int w, const int h, const char* title,
              const unsigned fs_targ_mon = 0)
{
  cout << "Obtaining OpenGL 3.3 core context" << std::endl;

  // Init GLFW
  if (!glfwInit()) {
    cerr << "\nERR: glfw: failed to init OGL/GLFW\n";
    exit(-1);
  }
  //
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  // Create a GLFW window in either windowed or fullscreen mode
  //--------------
  //
  if (!fs_targ_mon)  // Create in windowed mode
    mwin = glfwCreateWindow(w, h, title, nullptr, nullptr);
  else {  // Create in fullscreen mode, using the supplied monitor number
    int mon_cnt{};
    auto monitors{glfwGetMonitors(&mon_cnt)};
    if (static_cast<int>(fs_targ_mon) > mon_cnt) {
      cerr << "\nERR: glfw: selected full-screen monitor #" << fs_targ_mon
           << " not available\n";
      exit(-1);
    }
    mwin = glfwCreateWindow(w, h, title, monitors[fs_targ_mon - 1], nullptr);
  }
  //
  if (!mwin) {
    cerr << "\nERR: glfw: failed to create GLFW window\n";
    glfwTerminate();
    exit(-1);
  }
  //
  glfwMakeContextCurrent(mwin);

  init_system();
}

//------------------------------------------------------------------------------

// Additional system setup beyond the basic window
void init_system()
{
  // test
  // cout << "vulkan supported: " << glfwVulkanSupported() << '\n';

  // GLAD: load all OpenGL function pointers
  if (!gladLoadGLLoader(GLADloadproc(glfwGetProcAddress))) {
    cerr << "\nERR: glad: failed to get the OGL context process address\n";
    glfwDestroyWindow(mwin);
    glfwTerminate();
    exit(-1);
  }

  // Config global OpenGL state
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  // Set system callbacks
  glfwSetFramebufferSizeCallback(mwin, frmbuff_rsz_cb);
  glfwSetCursorPosCallback(mwin, mouse_cb);
  // glfwSetMouseButtonCallback(mwin, mouse_btn_cb);
  glfwSetKeyCallback(mwin, keyboard_cb);

  // MRS captures the mouse cursor by default
  glfwSetInputMode(mwin, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

  // Ensure the OpenGL viewport matches the actual system-assigned window
  // dimensions
  glfwGetFramebufferSize(mwin, &msys::mwin_w, &msys::mwin_h);
  glViewport(0, 0, msys::mwin_w, msys::mwin_h);

  glfwSwapInterval(1);  // A value of '1' should prevent screen tearing
}

//------------------------------------------------------------------------------

// Wrapper to initialize the scene elements
void init_scene()
{
  // Set the global scene camera's fixed-projection frustum
  msys::proj = glm::perspective(
      0.785398f,  // 45deg fov
      static_cast<float>(msys::mwin_w) / static_cast<float>(msys::mwin_h), 0.1f,
      100.0f);

  // Setup the training pavilion gym
  init_gym();

  // Setup the toys in the gym
  init_toys();

  // Setup the furniture in the gym
  // init_furniture();

  // Setup the text system
  //--------------
  //
  init_text_posns(msys::mwin_w, msys::mwin_h);  // NOTE: set this first
  //
  // TODO: Support generating/storing multiple font glyph textures/Text_char's
  init_text(48,
            "Roboto-Bold");  // Any valid TTF font probably works. (NOTE:
                             // the font file itself is expected to be
                             // inside the project's 'fonts' subdirectory)
  init_help_posns(msys::mwin_w, msys::mwin_h);

  // Init runtime var
  mtime::prev_sec = glfwGetTime();
}

//------------------------------------------------------------------------------

// DESIGN: Create a hex version of color handling in MRS?

// Clear the colorbuffer with a background color
void clr_buff(const float r = 0.27f, const float g = 0.37f,
              const float b = 0.57f,  // >1 defaults may need a newer compiler
              const float a = 1.0f)
{
  glClearColor(r, g, b, a);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

//------------------------------------------------------------------------------

// Wrapper to draw the elements of the scene
void draw_scene()
{
  // First get the global scene camera's current view matrix
  // NOTE: Basically every 3D drawable element in the MRS main window will also
  // use this view transform (to match the scene cam)
  msys::view = msys::scene_cam.GetViewMatrix();

  draw_gym();
}

//------------------------------------------------------------------------------

// Wrapper to draw the props (toys, furniture, etc.)
void draw_props()
{
  draw_toys();
  // draw_furniture();
}

//------------------------------------------------------------------------------

// Clean up the system, and release GLFW/OpenGL resources
void clean_up()
{
  // TODO: Also handle any other close-outs (write data files, etc) here
  // -(try not to screw Anon over if he accidentallys the sim. :^)

  glfwDestroyWindow(mwin);
  glfwTerminate();
}

//------------------------------------------------------------------------------

// Display final frame stats to the console
void prt_frm_stats(const double start_time, const double end_time)
{
  const double elpsd_secs{end_time - start_time};
  const double fps_rate{mtime::tot_frames / elpsd_secs};

  const std::locale loc{"en_US.UTF-8"};  // to separate digits by thousands
  cout.imbue(loc);                       //   "

  cout << "Rendered " << mtime::tot_frames << std::fixed << std::setprecision(1)
       << " frames in " << elpsd_secs << " seconds, at ~" << fps_rate
       << "fps average rate.\n";
}

//------------------------------------------------------------------------------

// Whenever the window size is changed (by OS or User resize) this callback is
// called
void frmbuff_rsz_cb(GLFWwindow*, int w, int h)
{
  // Ensure the OpenGL viewport matches the new window dimensions.
  glViewport(0, 0, w, h);
}

//------------------------------------------------------------------------------

// Adjust per-frame timing values
void do_timing()
{
  const auto curr_time{glfwGetTime()};

  mtime::delta_frm_time = curr_time - mtime::last_frm_time;
  mtime::last_frm_time = curr_time;

  // Update fps counter (once per second)
  if (curr_time - mtime::prev_sec >= 1.0) {  // immediately at 1 second elapsed,
    text::fps_cntr = to_string(mtime::fps_tick);  // fps_tick == the current fps

    // reset values, preparing for next second
    mtime::fps_tick = 0;
    mtime::prev_sec = curr_time;

    // just piggyback here for now to handle the double-tap esc timer & reset
    //--------------
    if (key::have_single_tap)
      if (++mtime::tap_secs >= mtime::tap_durn) {  // timer expired so reset
        mtime::tap_secs = 0;
        key::have_single_tap = false;
      }
  }

  ++mtime::fps_tick;
}

//------------------------------------------------------------------------------

// Wrapper to perform frame timing and draw scene elements
void time_n_draw()
{
  // Update per-frame timing & process realtime user inputs
  do_timing();
  if (key::key_prsd) proc_RT_inputs();
  // if (key::key_prsd || mouse::btn_prsd) proc_RT_inputs();

  // Clear the back colorbuffer
  clr_buff();

  // Update per-frame scene display
  draw_scene();

  // Update per-frame props display (toys, furniture, etc.)
  draw_props();

  // Update per-frame text displays
  draw_text();

  ++mtime::tot_frames;
}

//------------------------------------------------------------------------------

// Manage pausing the sim
void do_pause()
{
  // This scope enables us to only have to swap buffers once during each pause
  if (!pause::one_swap_guard) {
    //
    // on first pass, render a last frame for the sim, and save it's timepoint
    if (!pause::last_frm_guard) {
      time_n_draw();                        // render one last frame,
      pause::pause_moment = glfwGetTime();  // save the sim's restore point
      pause::last_frm_guard = true;
    }

    // Add any text displays into the back buffer before swapping
    //--------------
    //
    if (help::show_help) draw_help_scrn();
    //
    draw_tt_text("<paused>", pause::pause_x_pos, pause::pause_y_pos,
                 help::scale * 1.1, glm::vec3{1.0f, 0.0f, 0.0f});

    glfwSwapBuffers(mwin);

    pause::one_swap_guard = true;
  }

  // Prevent cpu-spin (pause this sim for 1/10th sec)
  std::this_thread::sleep_for(100ms);
}

//------------------------------------------------------------------------------

}  // namespace mrs

// Copyright (2020)
// License (MIT)  https://opensource.org/licenses/MIT
