// Say Hi to the MRS Anon,
// Muh Robowaifu Simulator
//========================
// The Young Wife Series:
// Chii-chan Edition
//

// filename: muh_text.hpp
// This file provides the facility to render quality TrueType font text to the
// screen. The font's glyph textures are generated using the FreeType library,
// and the pre-specified text's output positions are tailored for the MRS.

#pragma once

#include <iostream>
#include <map>
#include <string>
#include <utility>

//
#include "glad.h"

//
#include <glm/glm.hpp>

//
#include <ft2build.h>
#include FT_FREETYPE_H

//
#include "Camera.h"

//
#include "muh_Shader.hpp"
//
#include "muh_globals.hpp"
namespace text = mrs::text;
namespace msys = mrs::mrssys;
namespace key = mrs::input::key;

using std::cerr;
using std::string;
using std::to_string;

namespace mrs {

//------------------------------------------------------------------------------

// Set the x & y coordinates to render various on-screen texts
void init_text_posns(const unsigned w, const unsigned h)
{
  text::fps_y_pos = static_cast<float>(h - 35.0);

  text::dir_x_pos = static_cast<float>(w - 220.0);
  text::dir_y_pos = static_cast<float>(h - 35.0);

  text::dims_text = (to_string(w) + " x " + to_string(h));
  text::dims_x_pos =
      static_cast<float>((w / 2.0) - (w * .029));  // offset in -x
  text::dims_y_pos = static_cast<float>(h - 35.0);

  text::retcl_x_pos = static_cast<float>(w / 2.0);
  text::retcl_y_pos = static_cast<float>(h / 2.0);

  text::ortho_w = w;
  text::ortho_h = h;
}

//------------------------------------------------------------------------------

// Configure the VAO/VBO for the (single) text texture quad
void init_text_geo()
{
  // vao
  glGenVertexArrays(1, &text::text_vao);
  glBindVertexArray(text::text_vao);

  // vbo
  glGenBuffers(1, &text::text_vbo);
  glBindBuffer(GL_ARRAY_BUFFER, text::text_vbo);
  // 6 * 4 : 6 verts for quad- 2 sets of 2 floats each; for posn & cord
  glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 6 * 4, nullptr,
               GL_DYNAMIC_DRAW);
  // attr
  glEnableVertexAttribArray(0);
  // 4 : layout (location = 0) in vec4 vert;  // {vec2 posn, vec2 cord}
  glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);

  // unbinds
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);
}

//------------------------------------------------------------------------------

// Generate & store the first 128 glyph textures for the specified TrueType font
void init_text_texs(const unsigned font_sz, const std::string& font_nm)
{
  FT_Library ft_lib{};
  FT_Face ft_face{};

  // NOTE: All FT functions return a value other than 0 when an error occurs
  //

  if (FT_Init_FreeType(&ft_lib))
    cerr << "\nERR: FreeType: could not init freetype library\n";

  if (FT_New_Face(ft_lib, ("./fonts/" + font_nm + ".ttf").c_str(), 0, &ft_face))
    cerr << "\nERR: FreeType: failed to load font: '" << font_nm + ".ttf'\n";

  // Set size to load glyphs as
  FT_Set_Pixel_Sizes(ft_face, 0, font_sz);

  // Disable byte-alignment restriction  // DESIGN: Belongs here?
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

  // Only use the first 128 characters of ASCII set
  for (uint8_t c{0}; c < 128; ++c) {
    //
    // Load character glyph
    if (FT_Load_Char(ft_face, c, FT_LOAD_RENDER)) {
      cerr << "\nERR: FreeType: failed to load glyph\n";
      continue;
    }

    // Generate the glyph texture for this character
    //-----------------
    //
    unsigned curr_tex{};
    glGenTextures(1, &curr_tex);
    glBindTexture(GL_TEXTURE_2D, curr_tex);
    //
    // Args:
    // 1. Texture target (matching the GL_TEXTURE_2D from glBindTexture() above)
    // 2. Mipmap level. Left at base level 0.
    // 3. Format to store the texture.
    // 4&5. Obvs w & h, refers to texture result not input info.
    // 6. Weird legacy stuff, always set to 0.
    // 7. Source data format.
    // 8. Source data datatype. (chars == bytes)
    // 9. Finally, the actual face (image) data, as loaded by FreeType above.
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, ft_face->glyph->bitmap.width,
                 ft_face->glyph->bitmap.rows, 0, GL_RED, GL_UNSIGNED_BYTE,
                 ft_face->glyph->bitmap.buffer);
    //
    // Set texture options
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    //
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // struct Text_char {
    //   unsigned tex_ID;     // ID handle of the glyph texture
    //   glm::ivec2 size;     // Size of glyph
    //   glm::ivec2 bearing;  // Offset from baseline to left/top of glyph
    //   unsigned advance;    // Horizontal offset to advance to next glyph
    // };
    //
    // std::map<uint8_t, Text_char> text_chars{};
    //
    // Now store character for later use
    //-----------------
    //
    const text::Text_char text_char{
        curr_tex,
        glm::ivec2{ft_face->glyph->bitmap.width, ft_face->glyph->bitmap.rows},
        glm::ivec2{ft_face->glyph->bitmap_left, ft_face->glyph->bitmap_top},
        static_cast<unsigned>(ft_face->glyph->advance.x)};
    //
    text::text_chars.emplace(std::pair<uint8_t, text::Text_char>{c, text_char});

  }  // first 128 ASCII chars, 'c'

  // Cleanup FreeType resources
  FT_Done_Face(ft_face);
  FT_Done_FreeType(ft_lib);

  // unbind
  glBindTexture(GL_TEXTURE_2D, 0);

  // also assign the shader's sampler2D uniform to a texture unit
  text::txt_shdr.wrt_unifrm("text_samp", 0);
}

//------------------------------------------------------------------------------

// Compile & texture the OpenGL GLSL shader for use with text
// TODO: Support generating/storing multiple font glyph textures/Text_char's
void init_text(const unsigned font_sz = 48,
               const std::string font_nm = "Roboto-Bold")
{
  init_text_geo();

  // Create a special orthographic projection just for text
  glm::mat4 ortho_proj{glm::ortho(0.0f, static_cast<float>(text::ortho_w), 0.0f,
                                  static_cast<float>(text::ortho_h))};

  // Compile the shader and set it's projection uniform
  text::txt_shdr = Shader{"./shaders/text.vs", "./shaders/text.fs"};
  text::txt_shdr.use();
  text::txt_shdr.wrt_unifrm("proju", ortho_proj);

  // (compile shader first)
  init_text_texs(font_sz, font_nm);
}

//------------------------------------------------------------------------------

// Iterate through all the generated text VBO data and render glyphs onto screen
void draw_tt_text(const std::string& text, float x, const float y,
                  const float scale, const glm::vec3& color)
{
  text::txt_shdr.use();
  text::txt_shdr.wrt_unifrm("text_colru", color);

  // Activate corresponding render state
  glActiveTexture(GL_TEXTURE0);
  glBindVertexArray(text::text_vao);
  glBindBuffer(GL_ARRAY_BUFFER, text::text_vbo);

  // Iterate through all the chars in the input text string
  for (const auto c : text) {
    //
    // obtain the char's associated Text_char struct
    const auto txt_ch{text::text_chars[c]};

    // calculate the quad's vertex posn data from the Text_char and func params
    //-----------------
    const auto x_pos{x + txt_ch.bearing.x * scale};  // <- 'x' incr'd/loop iter
    const auto y_pos{y - (txt_ch.size.y - txt_ch.bearing.y) * scale};
    //
    const auto w{txt_ch.size.x * scale};
    const auto h{txt_ch.size.y * scale};

    // clang-format off

    // create customized VBO data for the character's quad using the posn data
    // [6][4] : 6 verts of 4 floats each, 2 for posn, 2 for cord
    //
    const float verts[6][4]{
      // posn x,y             // cord s,t (z,w in the vec4 text shader attr0)
      //                      //
      {x_pos,     y_pos + h,  0.0f, 0.0f},  // 1st tri
      {x_pos,     y_pos,      0.0f, 1.0f},
      {x_pos + w, y_pos,      1.0f, 1.0f},
      {x_pos,     y_pos + h,  0.0f, 0.0f},  // 2nd tri
      {x_pos + w, y_pos,      1.0f, 1.0f},
      {x_pos + w, y_pos + h,  1.0f, 0.0f}
    };

    // clang-format on

    // Update the contents of the VBO memory
    //
    // NOTE: We are creating brand-new vert data and updating the VBO memory
    // each time because in this current design, there is only allocated buffer
    // memory for a single quad's worth of verts--reused by all chars--residing
    // up on the GPU. We are therefore iteratively (re)sending new vert data for
    // each and every text character's underlying customized quad, each and
    // every frame, regardless of any changes to the overall text itself or not.
    //
    // Continuously streaming verts up to the GPU at draw-time like this is not
    // good--not to mention continuously (and redundantly) fully re-calculating
    // each text character's custom quad vert posns (also at draw-time).
    //
    // DESIGN: Fix this inefficient approach for handling text within the MRS.
    //
    glBufferSubData(  // NOTE: Use glBufferSubData, not glBufferData
        GL_ARRAY_BUFFER, 0, sizeof(verts), verts);

    // Finally, bind this char's texture and render it's custom quad
    glBindTexture(GL_TEXTURE_2D, txt_ch.tex_id);
    glDrawArrays(GL_TRIANGLES, 0, 6);

    // Now advance the x cursor for the next glyph
    // NOTE: The advance is in 1/64th pixels, ie. bitshift by 6 to get the value
    // in pixels (2^6 = 64)
    x += (txt_ch.advance >> 6) * scale;

  }  // all chars in input text, 'c'

  // unbinds
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);
  glBindTexture(GL_TEXTURE_2D, 0);

  // always good practice to set everything back to defaults once complete.
  glActiveTexture(GL_TEXTURE0);
}

//------------------------------------------------------------------------------

// Render the flying direction to the screen
void draw_fly_dir()
{
  using text::dir_text;

  switch (text::flying_dir) {
    case FORWARD:
      dir_text = text::dir_lbl + "FORWARD";
      break;
    case BACKWARD:
      dir_text = text::dir_lbl + "BACKWARD";
      break;
    case LEFT:
      dir_text = text::dir_lbl + "LEFT";
      break;
    case RIGHT:
      dir_text = text::dir_lbl + "RIGHT";
      break;
    case UP:
      dir_text = text::dir_lbl + "UP";
      break;
    case DOWN:
      dir_text = text::dir_lbl + "DOWN";
      break;
    default:
      dir_text = "";
  };

  draw_tt_text(dir_text, text::dir_x_pos, text::dir_y_pos, 0.32f,
               glm::vec3{0.0f, 0.0f, 0.0f});
}

//------------------------------------------------------------------------------

// Render the screen dimensions to the screen
void draw_scrn_dims()
{
  // TODO: add a text-scaling factor based on screen dims to keep the text
  // centered correctly regardless of screen resolution.
  // -use in the second term (the offset) in x_pos as well?

  draw_tt_text(text::dims_text, text::dims_x_pos, text::dims_y_pos, 0.3f,
               glm::vec3{0.0f, 0.0f, 0.0f});
}

//------------------------------------------------------------------------------

// Render a central reticle to the screen
void draw_reticle()
{
  draw_tt_text(text::retcl, text::retcl_x_pos, text::retcl_y_pos, 0.20f,
               glm::vec3{0.0f, 0.0f, 0.0f});
}

//------------------------------------------------------------------------------

// Render a central pip to the screen
void draw_pip()
{
  draw_tt_text(text::pip, text::retcl_x_pos, text::retcl_y_pos, 0.25f,
               glm::vec3{0.0f, 0.0f, 0.0f});
}

//------------------------------------------------------------------------------

// Render the full, labeled fps to the screen
void draw_full_fps()
{
  draw_tt_text(text::fps_lbl + text::fps_cntr, text::fps_x_pos, text::fps_y_pos,
               0.5f, glm::vec3{0.0f, 0.0f, 0.0f});
}

//------------------------------------------------------------------------------

// Render just the fps numerals to the screen
void draw_fps()
{
  draw_tt_text(text::fps_cntr, text::fps_x_pos, text::fps_y_pos, 0.35f,
               glm::vec3{0.0f, 0.0f, 0.0f});
}

//------------------------------------------------------------------------------

// Wrapper to render various text elements to the display
void draw_text()
{
  if (msys::show_min_disp) {  // Render only limited text elements
    draw_fps();
    draw_pip();
  }
  else {  // Render all on-screen text elements
    draw_full_fps();
    draw_reticle();
    if (key::key_prsd) draw_fly_dir();
    draw_scrn_dims();
  }
}

//------------------------------------------------------------------------------

}  // namespace mrs

// Copyright (2020)
// License (MIT)  https://opensource.org/licenses/MIT
