// Say Hi to the MRS Anon,
// Muh Robowaifu Simulator
//========================
// The Young Wife Series:
// Chii-chan Edition
//

// filename: mrs_main.cpp
// This file is the entry point for the MRS, and defines the game loop. The loop
// itself can be in either a paused or a normal state.

#include "muh_mrs.hpp"

int main()
{
  //-------------------
  // Prelims
  //

  // -Anon, try this config:
  //-----------------------------
  const unsigned w{1024}, h{576};
  mrs::init_win(w, h, "Muh Robowaifu Simulator");
  //
  // -or, try this one if you have the hardware:
  //  (comment one section out/uncomment the other in fact ofc :^)
  //-----------------------------
  // const unsigned w{1920}, h{1080};
  // mrs::init_win(w, h, "Muh Robowaifu Simulator", 2);

  // Prepare the scene for rendering
  mrs::init_scene();

  cout << "Begin game loop..." << std::endl;
  const auto start_time{glfwGetTime()};

  //-------------------
  // Game Loop
  //

  while (!glfwWindowShouldClose(mwin)) {
    glfwPollEvents();

    if (pause::paused)  // pause sim
      mrs::do_pause();
    //
    else {  // run sim normally
      mrs::time_n_draw();
      glfwSwapBuffers(mwin);
    }
  }

  //-------------------
  // Wrapups
  //

  const auto end_time{glfwGetTime()};
  cout << "Clean exit from game loop\n";

  mrs::clean_up();

  mrs::prt_frm_stats(start_time, end_time);
}

// Copyright (2020)
// License (MIT)  https://opensource.org/licenses/MIT
