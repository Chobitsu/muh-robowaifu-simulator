// Say Hi to the MRS Anon,
// Muh Robowaifu Simulator
//========================
// The Young Wife Series:
// Chii-chan Edition
//

// filename: muh_globals.hpp
// This file is a collection of variables that need to be either shared between
// functions, instantiated here for draw() performance optimizations, or just to
// be preserved between calls.
//
// They are corralled here as a convenient way to share them all among all the
// codebase, and partitioned off into sub-namespaces for precise specification
// and to avoid name collisions inside the mrs master namespace itself.
// (especially later on when it's yuge :^)

#pragma once

#include <map>

//
#include <GLFW/glfw3.h>

//
#include "Camera.h"
#include "Model.hpp"

//
#include "muh_Shader.hpp"

namespace mrs {

//------------------------------------------------------------------------------
namespace mrssys {

GLFWwindow* mwin{nullptr};  // the primary MRS window
// GLFWwindow* rwin{nullptr};  // the main robowaifu window
// GLFWwindow* cwin{nullptr};  // optional MRS control-panel window
// GLFWwindow* dwin{nullptr};  // optional MRS data/debug window

// Camera-related vars
//-------------------
Camera scene_cam{glm::vec3{0.0f, 0.0f, 3.0f}};
glm::mat4 proj{1.0f};
glm::mat4 view{1.0f};

//
int mwin_w{}, mwin_h{};

//
bool show_min_disp{true};

//
bool wire_on{false};

}  //  namespace mrssys

//------------------------------------------------------------------------------
namespace timing {

// frame timing for mouse/cam
float delta_frm_time{};
float last_frm_time{};

// fps counter display timing
double prev_sec{};
unsigned fps_tick{};

//
unsigned tot_frames{};

// esc key double-tap reset seconds counter & duration
unsigned tap_secs{};
const unsigned tap_durn{3};

}  //  namespace timing

//------------------------------------------------------------------------------
namespace input {

//--------------------------------------
namespace key {

bool key_prsd{false};

bool have_single_tap{false};

}  //  namespace key

//--------------------------------------
namespace mouse {

float last_x{};
float last_y{};
//
bool mouse_guard{true};

// Mouse cursor capture
bool crsr_captured{true};
bool crsr_recapture{false};

// bool btn_prsd{false};

}  //  namespace mouse
}  //  namespace input

//------------------------------------------------------------------------------
namespace text {

Shader txt_shdr{};

// Text glyph-related vars
//-------------------
// Holds all state information relevant to a text char as loaded using FreeType
struct Text_char {
  unsigned tex_id;     // ID handle of the glyph texture
  glm::ivec2 size;     // Size of glyph
  glm::ivec2 bearing;  // Offset from baseline to left/top of glyph
  unsigned advance;    // Horizontal offset to advance to next glyph
  // TODO: Add custom quad-layout info
};
//
std::map<uint8_t, Text_char> text_chars{};
unsigned text_vao{}, text_vbo{};
//
int ortho_w{}, ortho_h{};

const string fps_lbl{"fps rate:  "};
string fps_cntr{};
float fps_x_pos{35.0}, fps_y_pos{};

unsigned flying_dir{9'001};
const string dir_lbl{"Flying direction:   "};
string dir_text{};
float dir_x_pos{}, dir_y_pos{};

string dims_text{};
float dims_x_pos{}, dims_y_pos{};

const string retcl{"+"};
const string pip{"."};
float retcl_x_pos{}, retcl_y_pos{};

}  //  namespace text

//------------------------------------------------------------------------------
namespace help {

// Help-related vars
//-------------------
bool show_help{false}, pause_on_help{true};
float help_x_pos{}, help_y_pos{}, h_line_offset{}, scale{};

}  //  namespace help

//------------------------------------------------------------------------------
namespace pause {

// Pause-related vars
//-------------------
bool paused{false};
bool one_swap_guard{false};
bool last_frm_guard{false};
double pause_moment{};
float pause_x_pos{}, pause_y_pos{};

}  //  namespace pause

//------------------------------------------------------------------------------
namespace gym {

// Floor-related vars
//-------------------
Shader floor_shdr{};
unsigned floor_vao{};
unsigned floor_tex{};

}  //  namespace gym

//------------------------------------------------------------------------------
namespace toys {

// Toybox-related vars
//-------------------
Shader tbox_shdr{};
unsigned tbox_box_tex{}, tbox_face_tex{};
unsigned tbox_vao{};
//
const float tbox_scale{0.7f};
float colr_cycle{};
bool show_tbox{true};

// Moon-related vars
//-------------------
Shader moon_shdr{};
Model moon_modl{};
bool show_moon{true};

}  //  namespace toys

//------------------------------------------------------------------------------
namespace furn {

}  //  namespace furn

//------------------------------------------------------------------------------
namespace foo {

}  //  namespace foo

//------------------------------------------------------------------------------

}  // namespace mrs

// Copyright (2020)
// License (MIT)  https://opensource.org/licenses/MIT
