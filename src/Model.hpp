#pragma once

#include <filesystem>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

//
#include "glad.h"  // holds all OpenGL type declarations

//
#include <glm/glm.hpp>

//
#include <assimp/postprocess.h>
#include <assimp/scene.h>
//
#include <assimp/Importer.hpp>

//
#include "Mesh.hpp"

//
#include "muh_load_tex.hpp"

using std::begin;
using std::cerr;
using std::end;

//------------------------------------------------------------------------------

class Model {
 public:
  /*  Functions   */

  // muh default ctor
  Model() {}

  // ctor; expects a filepath to a 3D model, and a Shader
  //
  Model(const fs::path& mdl_path, Shader& shdr, bool gamma = false)
      : gamma_corr{gamma}
  {
    loadModel(mdl_path);
    assign_tex_units(shdr);
  }

  // Draws the model, and thus all its meshes
  void draw() const
  {
    for (const auto& mesh : meshes) mesh.draw();
  }

 private:
  /*  Model Data */

  // Stores all the textures loaded thus far, as an optimization to make sure
  // textures aren't loaded more than once.
  vector<Texture> all_loaded_texs{};

  vector<Mesh> meshes{};
  bool gamma_corr{};
  fs::path model_dir{};

  /*  Functions   */

  //-----
  // Setup the shader's texture uniforms for each mesh prior to draw()
  void assign_tex_units(Shader& shdr) const
  {
    for (const auto& mesh : meshes) mesh.assign_tex_units(shdr);
  }

  //-----
  // loads a model with supported ASSIMP extensions from file and stores the
  // resulting meshes in the meshes vector.
  void loadModel(const fs::path& mdl_path)
  {
    // read file via ASSIMP
    Assimp::Importer imptr;

    const auto scene{imptr.ReadFile(mdl_path, aiProcess_Triangulate |
                                                  aiProcess_FlipUVs |
                                                  aiProcess_CalcTangentSpace)};
    // check for errors
    if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE ||
        !scene->mRootNode)  // if is Not Zero
    {
      cerr << "\nerr assimp::importer: " << imptr.GetErrorString() << '\n';
      return;  // DESIGN: Some sort of error signal here?
    }

    // save the directory path of the model's filepath
    model_dir = mdl_path.parent_path();

    // process ASSIMP's root node recursively
    processNode(scene->mRootNode, scene);
  }

  //-----
  // processes a node in a recursive fashion. Processes each individual mesh
  // located at the node and repeats this process on its children nodes (if
  // any).
  void processNode(aiNode* node, const aiScene* scene)
  {
    // process each mesh located at the current node
    for (unsigned i{0}; i < node->mNumMeshes; ++i) {
      // the node object only contains indices to index the actual objects in
      // the scene. the scene contains all the data, node is just to keep stuff
      // organized (like relations between nodes).
      auto mesh{scene->mMeshes[node->mMeshes[i]]};
      meshes.push_back(processMesh(mesh, scene));
    }

    // after we've processed all of the meshes (if any) we then recursively
    // process each of the children nodes
    for (unsigned i{0}; i < node->mNumChildren; ++i) {
      processNode(node->mChildren[i], scene);
    }
  }

  //-----
  Mesh processMesh(aiMesh* mesh, const aiScene* scene)
  {
    // data to fill
    vector<Vertex> vertices{};
    vector<unsigned> indices{};
    vector<Texture> textures{};

    // Walk through each of the mesh's vertices
    for (unsigned i{0}; i < mesh->mNumVertices; ++i) {
      Vertex vertex{};
      glm::vec3 scratch{};  // we declare a placeholder vector since assimp
                            // uses its own vector class that doesn't directly
                            // convert to glm's vec3 class so we transfer the
                            // data to this placeholder glm::vec3 first.
      // positions
      scratch.x = mesh->mVertices[i].x;
      scratch.y = mesh->mVertices[i].y;
      scratch.z = mesh->mVertices[i].z;
      vertex.Position = scratch;

      // normals
      scratch.x = mesh->mNormals[i].x;
      scratch.y = mesh->mNormals[i].y;
      scratch.z = mesh->mNormals[i].z;
      vertex.Normal = scratch;

      // texture coordinates
      if (mesh->mTextureCoords[0])  // does the mesh contain texture
                                    // coordinates?
      {
        glm::vec2 vec{};

        // a vertex can contain up to 8 different texture coordinates. We thus
        // make the assumption that we won't use models where a vertex can have
        // multiple texture coordinates so we always take the first set (0).
        vec.x = mesh->mTextureCoords[0][i].x;
        vec.y = mesh->mTextureCoords[0][i].y;

        vertex.TexCoords = vec;
      }
      else
        vertex.TexCoords = glm::vec2(0.0f, 0.0f);

      // tangent
      scratch.x = mesh->mTangents[i].x;
      scratch.y = mesh->mTangents[i].y;
      scratch.z = mesh->mTangents[i].z;
      vertex.Tangent = scratch;

      // bitangent
      scratch.x = mesh->mBitangents[i].x;
      scratch.y = mesh->mBitangents[i].y;
      scratch.z = mesh->mBitangents[i].z;
      vertex.BiTangent = scratch;

      // Store the completed vertex
      vertices.push_back(vertex);
    }

    // now walk through each of the mesh's faces (a face is a mesh its triangle)
    // and retrieve the corresponding vertex indices.
    for (unsigned i{0}; i < mesh->mNumFaces; ++i) {
      auto face{mesh->mFaces[i]};

      // retrieve all indices of the face and store them in the indices vector
      for (unsigned j{0}; j < face.mNumIndices; ++j)
        indices.push_back(face.mIndices[j]);
    }

    // process materials
    //------------------

    auto material{scene->mMaterials[mesh->mMaterialIndex]};
    //
    // we assume a convention for sampler names in the shaders. Each diffuse
    // texture should be named as 'texture_diffuseN' where N is a sequential
    // number ranging from 1 to MAX_SAMPLER_NUMBER. Same applies to other
    // texture as the following list summarizes:
    // diffuse: texture_diffuseN
    // specular: texture_specularN
    // normal: texture_normalN
    // height: texture_heightN
    //
    // 1. diffuse maps
    vector<Texture> diffuseMaps{
        load_mat_texs(material, aiTextureType_DIFFUSE, "texture_diffuse")};
    textures.insert(end(textures), begin(diffuseMaps), end(diffuseMaps));
    //
    // 2. specular maps
    vector<Texture> specularMaps{
        load_mat_texs(material, aiTextureType_SPECULAR, "texture_specular")};
    textures.insert(end(textures), begin(specularMaps), end(specularMaps));
    //
    // 3. normal maps  // DESIGN: 'aiTextureType_HEIGHT' here??? ai has
    //                     'aiTextureType_NORMALS'
    std::vector<Texture> normalMaps{
        load_mat_texs(material, aiTextureType_HEIGHT, "texture_normal")};
    textures.insert(end(textures), begin(normalMaps), end(normalMaps));
    //
    // 4. height maps  // DESIGN: 'aiTextureType_AMBIENT' here??? ai has
    //                     'aiTextureType_HEIGHT'
    std::vector<Texture> heightMaps{
        load_mat_texs(material, aiTextureType_AMBIENT, "texture_height")};
    textures.insert(end(textures), begin(heightMaps), end(heightMaps));

    // return a mesh object created from the extracted mesh data
    return Mesh{vertices, indices, textures};
  }

  // // test:
  // void prt_tex_dat(const Texture& tex)
  // {
  //   std::cout << "tex.id: \t'" << tex.id << "'\n";
  //   std::cout << "tex.type: \t'" << tex.type << "'\n";
  //   std::cout << "tex.name: \t'" << tex.name << "'\n";
  // }

  //-----
  // Locate (and load as needed) all the material's textures of a given type
  vector<Texture> load_mat_texs(aiMaterial* const mat, const aiTextureType type,
                                const std::string& type_desc)
  {
    vector<Texture> texs_res{};

    // Instantiations of the scratch vars kept outside the loop(s)
    Texture new_tex{};
    aiString ai_path{};
    string curr_tex_name{};
    bool already_loaded{};

    for (unsigned i{0}; i < mat->GetTextureCount(type); ++i) {
      mat->GetTexture(type, i, &ai_path);
      curr_tex_name = ai_path.C_Str();

      // Check if a texture is already loaded and if so, just grab it's info
      // struct for return and continue with the next iteration.
      already_loaded = false;
      for (const auto& loaded_tex : all_loaded_texs) {
        if (loaded_tex.name == curr_tex_name) {
          texs_res.push_back(loaded_tex);
          already_loaded = true;
          break;
        }
      }
      //
      if (!already_loaded) {  // Go ahead and load the texture into the system,
                              // and store a new 'Texture' info struct for it.
        new_tex.id = mrs::load_tex(model_dir, curr_tex_name, gamma_corr);
        new_tex.type = type_desc;
        new_tex.name = curr_tex_name;
        texs_res.push_back(new_tex);

        // Also store it in the 'all textures loaded for the entire model', to
        // ensure we don't unnecessarily load duplicate textures again later.
        all_loaded_texs.push_back(new_tex);
      }
    }

    // // test:
    // std::cout << "all_loaded_texs.size(): '" << all_loaded_texs.size() <<
    // "'\n";
    // std::cout << "texs_res.size(): \t'" << texs_res.size() << "'\n";
    // for (const auto& tex : texs_res) prt_tex_dat(tex);
    // std::cout << std::endl;

    return texs_res;
  }

};  // class Model
