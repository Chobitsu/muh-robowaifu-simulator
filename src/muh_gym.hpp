// Say Hi to the MRS Anon,
// Muh Robowaifu Simulator
//========================
// The Young Wife Series:
// Chii-chan Edition
//

// filename: muh_gym.hpp
// This file is the training pavilion gym

#pragma once

//
#include "glad.h"

//
#include <glm/glm.hpp>

//
#include "muh_Shader.hpp"
#include "muh_load_tex.hpp"
//
#include "muh_globals.hpp"
namespace gym = mrs::gym;
namespace msys = mrs::mrssys;

namespace mrs {

//------------------------------------------------------------------------------

// set up vertex data (and buffer(s)) and configure vertex attributes
void init_floor_geo()
{
  // clang-format off

  const float floor_verts[]{
     // posn x,y,z           // nrml x,y,z       // cord s,t
     30.0f, -0.5f,  30.0f,   0.0f, 1.0f, 0.0f,   30.0f, 0.0f,  // 1st tri
    -30.0f, -0.5f,  30.0f,   0.0f, 1.0f, 0.0f,   0.0f,  0.0f,
    -30.0f, -0.5f, -30.0f,   0.0f, 1.0f, 0.0f,   0.0f,  30.0f,

     30.0f, -0.5f,  30.0f,   0.0f, 1.0f, 0.0f,   30.0f, 0.0f,  // 2nd tri
    -30.0f, -0.5f, -30.0f,   0.0f, 1.0f, 0.0f,   0.0f,  30.0f,
     30.0f, -0.5f, -30.0f,   0.0f, 1.0f, 0.0f,   30.0f, 30.0f
  };

  // clang-format on

  glGenVertexArrays(1, &gym::floor_vao);
  glBindVertexArray(gym::floor_vao);

  unsigned floor_vbo{};
  glGenBuffers(1, &floor_vbo);
  glBindBuffer(GL_ARRAY_BUFFER, floor_vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(floor_verts), floor_verts,
               GL_STATIC_DRAW);

  // posn attr
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);

  // nrml attr
  // glEnableVertexAttribArray(1);
  // glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float),
  //                       (void*)(3 * sizeof(float)));

  // cord attr
  glEnableVertexAttribArray(2);
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float),
                        (void*)(6 * sizeof(float)));
  // unbinds
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);
}

//------------------------------------------------------------------------------

//
void init_floor_tex()
{
  // Create texture for the floor
  //--------------
  const fs::path wood_file{"./resources/textures/wood.png"};
  gym::floor_tex = load_tex(wood_file, true);  // use gamma

  // also assign the shader's sampler2D uniform to a texture unit
  gym::floor_shdr.wrt_unifrm("floor_samp", 0);
}

//------------------------------------------------------------------------------

// Compile & texture the OpenGL GLSL shader for use with the floor geo
void init_floor()
{
  init_floor_geo();

  // Compile the shader and set it's projection uniform
  //--------------
  gym::floor_shdr = Shader{"./shaders/gym_floor.vs", "./shaders/gym_floor.fs"};
  gym::floor_shdr.use();
  gym::floor_shdr.wrt_unifrm("proju", msys::proj);

  // (compile shader first)
  init_floor_tex();
}

//------------------------------------------------------------------------------

// Render the gym floor
void draw_floor()
{
  // write the shader's dynamic uniform
  //--------------
  gym::floor_shdr.use();
  gym::floor_shdr.wrt_unifrm("viewu", msys::view);  // match the scene cam

  // render the floor
  //--------------
  //
  // bind texture to corresponding texture unit
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, gym::floor_tex);
  //
  glBindVertexArray(gym::floor_vao);
  glDrawArrays(GL_TRIANGLES, 0, 6);

  // unbinds
  glBindTexture(GL_TEXTURE_2D, 0);
  glBindVertexArray(0);
}

//------------------------------------------------------------------------------

void init_gym()
{
  init_floor();
  //
}

//------------------------------------------------------------------------------

void draw_gym()
{
  draw_floor();
  //
}

//------------------------------------------------------------------------------

}  // namespace mrs

// Copyright (2020)
// License (MIT)  https://opensource.org/licenses/MIT
