// Say Hi to the MRS Anon,
// Muh Robowaifu Simulator
//========================
// The Young Wife Series:
// Chii-chan Edition
//

// filename: muh_inputs.hpp
// This file is the key & mouse inputs

#pragma once

//
#include "glad.h"

//
#include <GLFW/glfw3.h>

//
#include "Camera.h"

//
#include "muh_globals.hpp"
namespace pause = mrs::pause;
namespace msys = mrs::mrssys;
namespace mtime = mrs::timing;
namespace mouse = mrs::input::mouse;
namespace key = mrs::input::key;
namespace text = mrs::text;

using key::key_prsd;
using msys::mwin;
using pause::paused;

namespace mrs {

//------------------------------------------------------------------------------

// Pause the simulation, called via either the help or pause keys
void set_pause()
{
  pause::one_swap_guard = false;
  paused = true;
}

//------------------------------------------------------------------------------

// Unpause the simulation, called via either the help or pause keys
void unset_pause()
{
  pause::last_frm_guard = false;
  glfwSetTime(
      pause::pause_moment);  // restore sim time back to moment we first paused

  paused = false;
}

//------------------------------------------------------------------------------

// Whenever a key is pressed on the keyboard, this callback is called
void keyboard_cb(GLFWwindow*, int glfw_key, int, int state, int /* mod */)
{
  // NOTE: state can also be '2' for repeat (also true)
  key_prsd = static_cast<bool>(state);

  switch (glfw_key) {
    //
    // Respond to esc key
    //--------------
    case GLFW_KEY_ESCAPE:
      if (key_prsd) {
        if (key::have_single_tap)  // have double-tap, so quick kill the program
          glfwSetWindowShouldClose(mwin, true);
        else {
          //
          // DESIGN: escape command mode(s)?
          // -be sure to reset key::have_single_tap & timer during handler
          //

          key::have_single_tap = true;
        }
      }
      break;

    // Toggle display of the help screen
    //--------------
    case GLFW_KEY_H:
    case GLFW_KEY_F1:
      if (key_prsd) {
        if (help::show_help) {
          help::show_help = false;
          if (paused) unset_pause();
        }
        else {
          help::show_help = true;
          if (help::pause_on_help) set_pause();
        }
      }
      break;

    // Toggle pause of the simulation
    //--------------
    case GLFW_KEY_SPACE:
      if (key_prsd) {
        if (paused) {
          if (help::show_help) help::show_help = false;
          unset_pause();
        }
        else
          set_pause();
      }
      break;

    // Toggle capture of the mouse cursor
    //--------------
    case GLFW_KEY_C:
      if (key_prsd) {
        if (mouse::crsr_captured) {
          glfwSetInputMode(mwin, GLFW_CURSOR, GLFW_CURSOR_NORMAL);  // uncapture
          mouse::crsr_captured = false;
        }
        else {
          glfwSetInputMode(mwin, GLFW_CURSOR, GLFW_CURSOR_DISABLED);  // capture
          mouse::crsr_recapture = mouse::crsr_captured = true;
        }
      }
      break;

    // Reset scene_cam
    //--------------
    case GLFW_KEY_BACKSPACE:
      if (key_prsd && !paused) msys::scene_cam.reset_cam();
      break;

    // Toggle display of the toybox
    //--------------
    case GLFW_KEY_X:
      if (key_prsd && !paused) {
        if (toys::show_tbox)
          toys::show_tbox = false;
        else
          toys::show_tbox = true;
      }
      break;

    // Toggle display of the moon
    //--------------
    case GLFW_KEY_M:
      if (key_prsd && !paused) {
        if (toys::show_moon)
          toys::show_moon = false;
        else
          toys::show_moon = true;
      }
      break;

    // Toggle minimal/full display of system information
    //--------------
    case GLFW_KEY_Z:
      if (key_prsd && !paused) {
        if (msys::show_min_disp)
          msys::show_min_disp = false;
        else
          msys::show_min_disp = true;
      }
      break;

    // Toggle fill mode or wireframe mode
    //--------------
    case GLFW_KEY_Q:
      if (key_prsd && !paused) {
        if (msys::wire_on) {
          glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);  // use fill mode
          msys::wire_on = false;
        }
        else {
          glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);  // use wireframe mode
          msys::wire_on = true;
        }
      }
      break;

      //--------------
    default:
      text::flying_dir = 9'001;  // the appropriate place to reset this
  };

  // RESERVES:
  // r - robowaifu (+actions ???) (eg; r+h+m == 'chii, hideki (point at) moon')
  // h - hideki (point at) (+anything)
  // l - lookat (+m (moon), +x (toybox), etc)
  // p - pickup (+item)
  // g - goto (+location or +item)
  // f - follow (+item or +character)
  //
  // del - (undo last command mode input?)
  // esc - (exit command mode w/o executing?)
  //
  // TOCHANGE:
  // -
}

//------------------------------------------------------------------------------

// Process inputs that need a faster response than the keyboard callback
void proc_RT_inputs()
{
  // Respond to flying keys:
  //
  // FWD
  //--------------
  if ((glfwGetKey(mwin, GLFW_KEY_W) == GLFW_PRESS) ||
      (glfwGetKey(mwin, GLFW_KEY_UP) == GLFW_PRESS)) {
    msys::scene_cam.ProcessKeyboard(FORWARD, mtime::delta_frm_time);
    text::flying_dir = FORWARD;
  }
  //
  // BACK
  //--------------
  if ((glfwGetKey(mwin, GLFW_KEY_S) == GLFW_PRESS) ||
      (glfwGetKey(mwin, GLFW_KEY_DOWN) == GLFW_PRESS)) {
    msys::scene_cam.ProcessKeyboard(BACKWARD, mtime::delta_frm_time);
    text::flying_dir = BACKWARD;
  }
  //
  // LEFT
  //--------------
  if ((glfwGetKey(mwin, GLFW_KEY_A) == GLFW_PRESS) ||
      (glfwGetKey(mwin, GLFW_KEY_LEFT) == GLFW_PRESS)) {
    msys::scene_cam.ProcessKeyboard(LEFT, mtime::delta_frm_time);
    text::flying_dir = LEFT;
  }
  //
  // RIGHT
  //--------------
  if ((glfwGetKey(mwin, GLFW_KEY_D) == GLFW_PRESS) ||
      (glfwGetKey(mwin, GLFW_KEY_RIGHT) == GLFW_PRESS)) {
    msys::scene_cam.ProcessKeyboard(RIGHT, mtime::delta_frm_time);
    text::flying_dir = RIGHT;
  }
  //
  // UP (key+LMB)  // DESIGN: eventually move this into the mouse btn cb
  //--------------
  if (glfwGetMouseButton(mwin, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS) {
    msys::scene_cam.ProcessKeyboard(UP, mtime::delta_frm_time);
    text::flying_dir = UP;
  }
  //
  // DOWN (key+RMB)  // DESIGN: eventually move this into the mouse btn cb
  //--------------
  if (glfwGetMouseButton(mwin, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS) {
    msys::scene_cam.ProcessKeyboard(DOWN, mtime::delta_frm_time);
    text::flying_dir = DOWN;
  }
}

//------------------------------------------------------------------------------

// *  @param[in] button The [mouse button](@ref buttons) that was pressed or
// *  released.
// *  @param[in] action One of `GLFW_PRESS` or `GLFW_RELEASE`.
// *  @param[in] mods Bit field describing which [modifier keys](@ref mods) were
// *  held down.
//
// void mouse_btn_cb(GLFWwindow*, int btn, int state, int mod)
// {
//   // test:
//   std::cout << "btn: " << btn << " state: " << state << " mod: " << mod
//             << std::endl;

//   if (state == GLFW_PRESS)
//     mouse::btn_prsd = true;
//   else
//     mouse::btn_prsd = false;
// }

//------------------------------------------------------------------------------

// Whenever the mouse moves, this callback is called
void mouse_cb(GLFWwindow*, double x_pos, double y_pos)
{
  if (mouse::mouse_guard) {  // init on first pass
    mouse::last_x = x_pos;
    mouse::last_y = y_pos;
    mouse::mouse_guard = false;
  }

  if (!paused &&
      mouse::crsr_captured) {  // only process mouse movements if needed

    float x_offset{}, y_offset{};

    if (mouse::crsr_recapture) {   // ignore any real offsets during the first
      x_offset = y_offset = 0.0f;  // tick after a cursor recapture
      mouse::crsr_recapture = false;
    }
    else {
      x_offset = x_pos - mouse::last_x;
      // reversed since y-coordinates go from bottom to top
      y_offset = mouse::last_y - y_pos;
    }

    msys::scene_cam.ProcessMouseMovement(x_offset, y_offset);
  }

  mouse::last_x = x_pos;
  mouse::last_y = y_pos;
}

}  // namespace mrs

// Copyright (2020)
// License (MIT)  https://opensource.org/licenses/MIT
