// Say Hi to the MRS Anon,
// Muh Robowaifu Simulator
//========================
// The Young Wife Series:
// Chii-chan Edition
//

// filename: muh_toys.hpp
// This file is the children's toys for the training pavilion gym. Balls and
// blocks and a toybox, etc.

#pragma once

#include <cmath>
#include <filesystem>

//
#include "glad.h"

//
#include <GLFW/glfw3.h>

//
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

//
#include "Model.hpp"

//
#include "muh_Shader.hpp"
#include "muh_load_tex.hpp"
//
#include "muh_globals.hpp"
namespace toys = mrs::toys;
namespace msys = mrs::mrssys;

namespace fs = std::filesystem;

using std::sin;

namespace mrs {

//------------------------------------------------------------------------------

// Setup vertex data (and buffer(s)) and configure vertex attributes
void init_toybox_geo()
{
  const float st_lo1{0.0}, st_hi1{1.0}, st_hi2{3.0};

  // clang-format off

  // Vertex data for the toybox's vertex buffer object
  float tbox_verts[] = {
     // x,y,z posn         // r,g,b colr       // s,t cord

        // Front face (+z)
    -0.5f, -0.5f,  0.5f,   0.0f, 0.0f, 1.0f,   st_lo1, st_lo1, // tri 1
     0.5f, -0.5f,  0.5f,   0.0f, 1.0f, 1.0f,   st_hi2, st_lo1,
     0.5f,  0.5f,  0.5f,   0.0f, 1.0f, 0.0f,   st_hi2, st_hi2,
     0.5f,  0.5f,  0.5f,   1.0f, 1.0f, 0.0f,   st_hi2, st_hi2, // tri 2
    -0.5f,  0.5f,  0.5f,   1.0f, 0.0f, 0.0f,   st_lo1, st_hi2,
    -0.5f, -0.5f,  0.5f,   1.0f, 0.0f, 1.0f,   st_lo1, st_lo1,

        // Left face (-x)
    -0.5f,  0.5f,  0.5f,   0.0f, 0.0f, 1.0f,   st_lo1, st_hi1, // tri 1
    -0.5f,  0.5f, -0.5f,   0.0f, 1.0f, 1.0f,   st_hi1, st_hi1,
    -0.5f, -0.5f, -0.5f,   0.0f, 1.0f, 0.0f,   st_hi1, st_lo1,
    -0.5f, -0.5f, -0.5f,   1.0f, 1.0f, 0.0f,   st_hi1, st_lo1, // tri 2
    -0.5f, -0.5f,  0.5f,   1.0f, 0.0f, 0.0f,   st_lo1, st_lo1,
    -0.5f,  0.5f,  0.5f,   1.0f, 0.0f, 1.0f,   st_lo1, st_hi1,

        // Back face (-z)
    -0.5f, -0.5f, -0.5f,   0.0f, 0.0f, 1.0f,   st_lo1, st_lo1, // tri 1
     0.5f, -0.5f, -0.5f,   0.0f, 1.0f, 1.0f,   st_hi2, st_lo1,
     0.5f,  0.5f, -0.5f,   0.0f, 1.0f, 0.0f,   st_hi2, st_hi2,
     0.5f,  0.5f, -0.5f,   1.0f, 1.0f, 0.0f,   st_hi2, st_hi2, // tri 2
    -0.5f,  0.5f, -0.5f,   1.0f, 0.0f, 0.0f,   st_lo1, st_hi2,
    -0.5f, -0.5f, -0.5f,   1.0f, 0.0f, 1.0f,   st_lo1, st_lo1,

        // Right face (+x)
     0.5f,  0.5f,  0.5f,   0.0f, 0.0f, 1.0f,   st_lo1, st_hi1, // tri 1
     0.5f,  0.5f, -0.5f,   0.0f, 1.0f, 1.0f,   st_hi1, st_hi1,
     0.5f, -0.5f, -0.5f,   0.0f, 1.0f, 0.0f,   st_hi1, st_lo1,
     0.5f, -0.5f, -0.5f,   1.0f, 1.0f, 0.0f,   st_hi1, st_lo1, // tri 2
     0.5f, -0.5f,  0.5f,   1.0f, 0.0f, 0.0f,   st_lo1, st_lo1,
     0.5f,  0.5f,  0.5f,   1.0f, 0.0f, 1.0f,   st_lo1, st_hi1,

        // Top face (+y)
    -0.5f,  0.5f, -0.5f,   0.0f, 0.0f, 1.0f,   st_lo1, st_hi1, // tri 1
     0.5f,  0.5f, -0.5f,   0.0f, 1.0f, 1.0f,   st_hi1, st_hi1,
     0.5f,  0.5f,  0.5f,   0.0f, 1.0f, 0.0f,   st_hi1, st_lo1,
     0.5f,  0.5f,  0.5f,   1.0f, 1.0f, 0.0f,   st_hi1, st_lo1, // tri 2
    -0.5f,  0.5f,  0.5f,   1.0f, 0.0f, 0.0f,   st_lo1, st_lo1,
    -0.5f,  0.5f, -0.5f,   1.0f, 0.0f, 1.0f,   st_lo1, st_hi1,

        // Bottom face (-y)
    -0.5f, -0.5f, -0.5f,   0.0f, 0.0f, 1.0f,   st_lo1, st_hi2, // tri 1
     0.5f, -0.5f, -0.5f,   0.0f, 1.0f, 1.0f,   st_hi2, st_hi2,
     0.5f, -0.5f,  0.5f,   0.0f, 1.0f, 0.0f,   st_hi2, st_lo1,
     0.5f, -0.5f,  0.5f,   1.0f, 1.0f, 0.0f,   st_hi2, st_lo1, // tri 2
    -0.5f, -0.5f,  0.5f,   1.0f, 0.0f, 0.0f,   st_lo1, st_lo1,
    -0.5f, -0.5f, -0.5f,   1.0f, 0.0f, 1.0f,   st_lo1, st_hi2,
  };

  // clang-format on

  glGenVertexArrays(1, &toys::tbox_vao);
  glBindVertexArray(toys::tbox_vao);

  unsigned tbox_vbo{};
  glGenBuffers(1, &tbox_vbo);
  glBindBuffer(GL_ARRAY_BUFFER, tbox_vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(tbox_verts), tbox_verts, GL_STATIC_DRAW);

  // posn attr (loc# 0); 3 elements at the 0-th data offset.
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);

  // colr attr (loc# 1); 3 elements at the 3-th data offset.
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float),
                        (void*)(3 * sizeof(float)));

  // cord attr (loc# 2); 2 elements at the 6-th data offset.
  glEnableVertexAttribArray(2);
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float),
                        (void*)(6 * sizeof(float)));
  // unbinds
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);
}

//------------------------------------------------------------------------------

//
void init_tbox_texs()
{
  // Create two textures for toybox
  //--------------
  const fs::path box_file{"./resources/textures/container.jpg"};
  const fs::path face_file{"./resources/textures/awesomeface.png"};
  //
  toys::tbox_box_tex = load_tex(box_file);
  toys::tbox_face_tex = load_tex(face_file, true, true);  // use gamma & flipy

  // also assign each of the shader's sampler2D uniforms to a texture unit
  toys::tbox_shdr.wrt_unifrm("box_samp", 0);
  toys::tbox_shdr.wrt_unifrm("face_samp", 1);
}

//------------------------------------------------------------------------------

// Compile & texture the OpenGL GLSL shader for use with the toybox geo
void init_toybox()
{
  init_toybox_geo();

  // Compile the shader and set it's projection uniform
  //--------------
  toys::tbox_shdr = Shader{"./shaders/toybox.vs", "./shaders/toybox.fs"};
  toys::tbox_shdr.use();
  toys::tbox_shdr.wrt_unifrm("proju", msys::proj);

  // (compile shader first)
  init_tbox_texs();
}

//------------------------------------------------------------------------------

// Render the rotating toybox
void draw_toybox()
{
  // assign transformations
  //--------------
  //
  // init the object's model transform to identity
  glm::mat4 modl{1.0f};
  //
  // Keep from having floor inter-penetration
  modl = glm::translate(modl, glm::vec3{0.0f, -0.149f, 0.0f});
  //
  const float time_radian = glfwGetTime() / 4;
  modl = glm::rotate(modl, time_radian, glm::vec3{0.0f, 1.0f, 0.0f});
  //
  modl = glm::scale(
      modl, glm::vec3(toys::tbox_scale, toys::tbox_scale, toys::tbox_scale));

  // cycle the color blend (1.0->0.0->1.0... over several secs)
  toys::colr_cycle = static_cast<float>(sin(glfwGetTime() / 3.3) / 2.0) + 0.5f;

  // write the shader's dynamic uniforms
  //--------------
  toys::tbox_shdr.use();
  toys::tbox_shdr.wrt_unifrm("modlu", modl);
  toys::tbox_shdr.wrt_unifrm("viewu", msys::view);  // match the scene cam
  toys::tbox_shdr.wrt_unifrm("colr_mixu", toys::colr_cycle);

  // render the toybox
  //--------------
  //
  // bind textures to corresponding texture units
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, toys::tbox_box_tex);
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, toys::tbox_face_tex);
  //
  glBindVertexArray(toys::tbox_vao);
  glDrawArrays(GL_TRIANGLES, 0, 36);

  // unbinds
  glBindTexture(GL_TEXTURE_2D, 0);
  glBindVertexArray(0);

  // always good practice to set everything back to defaults once complete.
  glActiveTexture(GL_TEXTURE0);
}

//------------------------------------------------------------------------------

// Muh 'Exoplanet non-existent-surface-image MoonBall' in fact of course. :^)
void init_moon()
{
  // Compile the shader and set it's projection uniform
  //--------------
  toys::moon_shdr =
      Shader{"./shaders/model_load.vs", "./shaders/model_load.fs"};
  toys::moon_shdr.use();
  toys::moon_shdr.wrt_unifrm("proju", msys::proj);

  // Setup everything for our 'Moon' by loading it's 3D model straight from disk
  //--------------
  const fs::path moon_mdl_path{"./resources/objects/planet/planet.obj"};
  toys::moon_modl = Model{moon_mdl_path, toys::moon_shdr};
}

//------------------------------------------------------------------------------

// Render the orbiting moon
void draw_moon()
{
  // assign transformations
  //--------------
  //
  // init the object's model transform to identity
  glm::mat4 modl{1.0f};
  //
  const float time_radian = glfwGetTime() / 48;
  modl = glm::rotate(modl, time_radian, glm::vec3{0.5f, 0.5f, 0.5f});
  //
  modl = glm::translate(modl, glm::vec3{-15.0f, 40.0f, -32.0f});

  // write the shader's dynamic uniforms
  //--------------
  toys::moon_shdr.use();
  toys::moon_shdr.wrt_unifrm("modlu", modl);
  toys::moon_shdr.wrt_unifrm("viewu", msys::view);  // match the scene cam

  // render the moon
  toys::moon_modl.draw();
}

//------------------------------------------------------------------------------

// Wrapper for initializing all the toys and toy accessories.
void init_toys()
{
  init_toybox();
  init_moon();
}

//------------------------------------------------------------------------------

// Wrapper for rendering all the toys and toy accessories to the screen.
void draw_toys()
{
  if (toys::show_tbox) draw_toybox();
  if (toys::show_moon) draw_moon();
}

//------------------------------------------------------------------------------

}  // namespace mrs

// Copyright (2020)
// License (MIT)  https://opensource.org/licenses/MIT
