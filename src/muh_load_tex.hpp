// Say Hi to the MRS Anon,
// Muh Robowaifu Simulator
//========================
// The Young Wife Series:
// Chii-chan Edition
//

// filename: muh_loadtex.hpp
// This file is the general texture-loading utility

#pragma once

#include <filesystem>
#include <iostream>
#include <string>

//
#include "glad.h"

//
#include "stb_image.h"

namespace fs = std::filesystem;

using std::cerr;

namespace mrs {

//------------------------------------------------------------------------------

// Utility function for loading a 2D texture from file
unsigned load_tex(const fs::path& path, const bool gamma_corr = false,
                  const bool flip_y = false,
                  const GLenum repeat_mode = GL_REPEAT)
{
  unsigned tex_id{};
  glGenTextures(1, &tex_id);

  // tell stb_image to flip loaded texture on the y-axis.
  if (flip_y) stbi_set_flip_vertically_on_load(true);

  int w{}, h{}, num_chans{};
  auto data{stbi_load(path.c_str(), &w, &h, &num_chans, 0)};

  if (data) {
    GLenum tex_out_fmt{}, src_in_fmt{};

    if (num_chans == 1) {
      tex_out_fmt = src_in_fmt = GL_RED;
    }
    else if (num_chans == 3) {
      tex_out_fmt = gamma_corr ? GL_SRGB : GL_RGB;
      src_in_fmt = GL_RGB;
    }
    else {  // (num_chans == 4)
      tex_out_fmt = gamma_corr ? GL_SRGB_ALPHA : GL_RGBA;
      src_in_fmt = GL_RGBA;
    }

    glBindTexture(GL_TEXTURE_2D, tex_id);
    //
    // Args:
    // 1. Texture target (matching the GL_TEXTURE_2D from glBindTexture() above)
    // 2. Mipmap level. Left at base level 0 for now.
    // 3. Format to store the texture.
    // 4&5. Obvs w & h, refers to texture result not input info. We reuse the
    // dims provided by the file image.
    // 6. Weird legacy stuff, always set to 0.
    // 7. Source image format.
    // 8. Source image datatype. (chars == bytes)
    // 9. Finally, the actual image data, as loaded by stbi above.
    glTexImage2D(GL_TEXTURE_2D, 0, tex_out_fmt, w, h, 0, src_in_fmt,
                 GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, repeat_mode);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, repeat_mode);
    //
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                    GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  }
  else
    cerr << "nERR: failed to load texture file: " << path << '\n';

  stbi_image_free(data);

  return tex_id;
}

//------------------------------------------------------------------------------

// Wrapper to concatenate directory + texture name. Used via Model.hpp
unsigned load_tex(const fs::path& directory, const std::string& tex_nm,
                  const bool gamma_corr = false)
{
  const fs::path full_path{directory / tex_nm};

  return load_tex(full_path, gamma_corr);
}

//------------------------------------------------------------------------------

}  // namespace mrs

// Copyright (2020)
// License (MIT)  https://opensource.org/licenses/MIT
