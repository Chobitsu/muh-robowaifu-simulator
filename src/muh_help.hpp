// Say Hi to the MRS Anon,
// Muh Robowaifu Simulator
//========================
// The Young Wife Series:
// Chii-chan Edition
//

// filename: muh_help.hpp
// This file is the help system

#pragma once

#include <chrono>
#include <string>
#include <thread>
#include <utility>
#include <vector>

//
#include "glad.h"

//
#include <GLFW/glfw3.h>

//
#include <glm/glm.hpp>

//
#include "muh_text.hpp"
//
#include "muh_globals.hpp"
namespace text = mrs::text;
namespace help = mrs::help;
namespace pause = mrs::pause;

using namespace std::chrono_literals;

using std::pair;
using std::string;
using std::vector;

namespace mrs {

//------------------------------------------------------------------------------

// Set the x & y coordinates to render help text
void init_help_posns(const unsigned w, const unsigned h)
{
  help::scale = 0.5 * (w / 1024.0);

  help::help_x_pos = w * 0.05;
  help::help_y_pos = static_cast<float>(h - (h * 0.12));

  help::h_line_offset = (h / 17.5);

  // also specify the pause posn while we're at it
  pause::pause_x_pos = (w / 2.0) - (w * 0.04);
  pause::pause_y_pos = static_cast<float>(0 + (h * 0.03));
}

//------------------------------------------------------------------------------

// Wrapper to allow a pair of floats for the text position
void draw_tt_text(const std::string& text,
                  const std::pair<float, float>& text_posn, const float scale,
                  const glm::vec3& color)
{
  const auto [x_posn, y_posn]{text_posn};
  draw_tt_text(text, x_posn, y_posn, scale, color);
}

//------------------------------------------------------------------------------

// Render help text elements to the display
void draw_help_scrn()
{
  const glm::vec3 help_color{0.07f, 0.75f, 0.95f};
  pair<float, float> line_posn{help::help_x_pos, help::help_y_pos};

  const vector<string> help_lines{
      {"[w,a,s,d] or [arrow keys] - fly camera"},
      {"[key + lmb/rmb] - fly up/down"},
      {"[x] - toggle toybox display"},
      {"[m] - toggle moon display"},
      {"[z] - toggle minimal info display"},
      {"[f1] or [h] - toggle help display"},
      {"[q] - toggle wireframe mode"},
      {"[c] - toggle mouse cursor capture"},
      {"[space] - toggle simulation pause"},
      {"[bkspc] - reset scene camera"},
      {"[esc 2x] - double-tap (3s) exits program"},
      {"advancing robotics to a point where anime catgrill meidos in tiny "
       "miniskirts are a reality"},
      {"advancing robotics to a point where anime catgrill meidos in tiny "
       "miniskirts are a reality"},
      {"advancing robotics to a point where anime catgrill meidos in tiny "
       "miniskirts are a reality"},
      {"advancing robotics to a point where anime catgrill meidos in tiny "
       "miniskirts are a reality"}};

  for (const auto& line : help_lines) {
    //
    draw_tt_text(line, line_posn, help::scale, help_color);
    line_posn.second -= help::h_line_offset;
  }
}

//------------------------------------------------------------------------------

}  // namespace mrs

// Copyright (2020)
// License (MIT)  https://opensource.org/licenses/MIT
