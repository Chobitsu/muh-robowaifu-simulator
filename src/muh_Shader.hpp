// Say Hi to the MRS Anon,
// Muh Robowaifu Simulator
//========================
// The Young Wife Series:
// Chii-chan Edition
//

// filename: 'muh_Shader.hpp'
// -This file is the MRS-style implementation of the Shader class from
// learnopengl

#pragma once

#include <filesystem>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

//
#include "glad.h"

//
#include <glm/glm.hpp>

using std::cerr;
using std::ifstream;
using std::string;
using std::stringstream;

namespace fs = std::filesystem;

//------------------------------------------------------------------------------

std::string read_file(const fs::path&);
unsigned build_shader(const std::string&, const std::string&);

class Shader {
 public:
  Shader() {}

  Shader(const fs::path& vert_path, const fs::path& frag_path)
  {
    const auto vs_code{read_file(vert_path)};
    const auto fs_code{read_file(frag_path)};

    id_ = build_shader(vs_code, fs_code);
  }

  // Any of these might be called during draw()s:
  //------------------

  inline void use() const { glUseProgram(id_); }
  //
  inline void wrt_unifrm(const char* name, const float val) const
  {
    glUniform1f(glGetUniformLocation(id_, name), val);
  }
  inline void wrt_unifrm(const char* name, const int val) const
  {
    glUniform1i(glGetUniformLocation(id_, name), val);
  }
  inline void wrt_unifrm(const char* name, const unsigned val) const
  {
    glUniform1ui(glGetUniformLocation(id_, name), val);  // ui supported/shader?
  }
  inline void wrt_unifrm(const char* name, const bool val) const
  {
    glUniform1i(glGetUniformLocation(id_, name), static_cast<int>(val));
  }
  inline void wrt_unifrm(const char* name, const glm::vec3& val) const
  {
    glUniform3fv(glGetUniformLocation(id_, name), 1, &val[0]);
  }
  inline void wrt_unifrm(const char* name, const glm::mat4& val) const
  {
    glUniformMatrix4fv(glGetUniformLocation(id_, name), 1, GL_FALSE,
                       &val[0][0]);
  }
  //
  inline int get_loc(const char* name) const
  {
    return glGetUniformLocation(id_, name);
  }
  //
  inline unsigned id() const { return id_; }

 private:
  unsigned id_{};
};

//------------------------------------------------------------------------------

void set_ifstream_bits(ifstream& ifs)
{
  ifs.exceptions(ifstream::failbit | ifstream::badbit);
}

//------------------------------------------------------------------------------

std::string read_file(const fs::path& shdr_path)
{
  ifstream ifs{};
  set_ifstream_bits(ifs);

  stringstream buff{};
  string res{};

  try {
    ifs.open(shdr_path);
    buff << ifs.rdbuf();
    res = buff.str();
  }
  catch (const ifstream::failure& fs_err) {
    cerr << "\nERR: Shader: shader_file_not_read:\n" << shdr_path << '\n';
  }

  return res;
}

//------------------------------------------------------------------------------

// Display the type of shader build error--and the error log--to the user
void display_err(const unsigned shader, const std::string& type_lbl)
{
  string action{};
  const unsigned log_sz{4096};
  char err_log[log_sz]{};

  if (type_lbl == "program") {
    action = "linking";
    glGetProgramInfoLog(shader, log_sz, nullptr, err_log);
  }
  else {  // type is a vert or frag shader
    action = "compilation";
    glGetShaderInfoLog(shader, log_sz, nullptr, err_log);
  }

  cerr << "\nERR: shader: " << type_lbl << " " << action << " failed\n"
       << err_log << '\n';
}

//------------------------------------------------------------------------------

// Shader build result status
int shdr_success{};

// Check for a successful shader compile
void chk_compile_status(const unsigned shader, const std::string& type_lbl)
{
  glGetShaderiv(shader, GL_COMPILE_STATUS, &shdr_success);
  if (!shdr_success) display_err(shader, type_lbl);
}

// Check for a successful shader link
void chk_link_status(const unsigned shader)
{
  glGetProgramiv(shader, GL_LINK_STATUS, &shdr_success);
  if (!shdr_success) display_err(shader, "program");
}

//------------------------------------------------------------------------------

// Compile either a vertex or fragment shader
unsigned compile_shader(const std::string& code, const uint type)
{
  const auto shader{glCreateShader(type)};
  const auto shader_source{code.c_str()};

  glShaderSource(shader, 1, &shader_source, nullptr);
  glCompileShader(shader);

  const auto type_lbl{type == GL_VERTEX_SHADER ? "vertex" : "fragment"};
  chk_compile_status(shader, type_lbl);

  return shader;
}

//------------------------------------------------------------------------------

// Link the two shaders together to form our shader program
unsigned link_shdr_pgrm(const unsigned vert_shdr, const unsigned frag_shdr)
{
  const auto shdr_pgrm{glCreateProgram()};

  glAttachShader(shdr_pgrm, vert_shdr);
  glAttachShader(shdr_pgrm, frag_shdr);
  glLinkProgram(shdr_pgrm);
  //
  chk_link_status(shdr_pgrm);

  return shdr_pgrm;
}

//------------------------------------------------------------------------------

// Build our GLSL shader program
unsigned build_shader(const std::string& vs_code, const std::string& fs_code)
{
  const auto vert_shdr{compile_shader(vs_code, GL_VERTEX_SHADER)};
  const auto frag_shdr{compile_shader(fs_code, GL_FRAGMENT_SHADER)};

  const auto shader{link_shdr_pgrm(vert_shdr, frag_shdr)};

  glDeleteShader(vert_shdr);
  glDeleteShader(frag_shdr);

  return shader;
}

//------------------------------------------------------------------------------

// Copyright (2020)
// License (MIT)  https://opensource.org/licenses/MIT
