// Say Hi to the MRS Anon,
// Muh Robowaifu Simulator
//========================
// The Young Wife Series:
// Chii-chan Edition
//

// filename: muh_furniture.hpp
// This file is the furniture for the training pavilion gym. Table, chairs,
// couch, etc.

#pragma once

// GLAD
#include "glad.h"

//
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace mrs {

//------------------------------------------------------------------------------

void init_furniture_geo() {}

//------------------------------------------------------------------------------

void init_furniture() { init_furniture_geo(); }

//------------------------------------------------------------------------------

void draw_furniture() {}

//------------------------------------------------------------------------------

}  // namespace mrs

// Copyright (2020)
// License (MIT)  https://opensource.org/licenses/MIT
