#pragma once

#include <cstddef>
#include <string>
#include <vector>

//
#include "glad.h"  // holds all OpenGL type declarations

//
#include <glm/glm.hpp>

//
#include "muh_Shader.hpp"

using std::string;
using std::to_string;
using std::vector;

struct Vertex {
  glm::vec3 Position;
  glm::vec3 Normal;
  glm::vec2 TexCoords;
  glm::vec3 Tangent;
  glm::vec3 BiTangent;
};

struct Texture {
  unsigned id;
  string type;
  string name;
};

//------------------------------------------------------------------------------

class Mesh {
 public:
  /*  Functions  */

  // ctor
  Mesh(std::vector<Vertex>& vertices, std::vector<unsigned>& indices,
       std::vector<Texture>& textures)
      : vertices{vertices}, indices{indices}, textures{textures}
  {
    // Set the vertex buffers and the attribute pointers.
    setupMesh();
  }

  // Setup the shader's texture uniforms for this mesh prior to draw()
  void assign_tex_units(Shader& shader) const
  {
    string type{}, num{};
    unsigned diffuse_num{1}, specular_num{1}, normal_num{1}, height_num{1};

    shader.use();

    // For each texture in the mesh, assign the shader's sampler2d uniform to
    // a matching-indexed texture unit
    for (unsigned i{0}; i < textures.size(); ++i) {
      //
      // retrieve texture number (the 'N' in diffuse_textureN)
      type = textures[i].type;
      if (type == "texture_diffuse")
        num = to_string(diffuse_num++);
      else if (type == "texture_specular")
        num = to_string(specular_num++);
      else if (type == "texture_normal")
        num = to_string(normal_num++);
      else if (type == "texture_height")
        num = to_string(height_num++);

      // write the shader's sampler2D uniform to assign its matched texture unit
      // DESIGN: Greater than 16 textures possible for a mesh? If so, is that
      // valid for a texture unit number assignment?
      shader.wrt_unifrm((type + num).c_str(), i);
    }
  }

  // Render the mesh
  void draw() const
  {
    // For each texture in the mesh
    for (unsigned i{0}; i < textures.size(); ++i) {
      //
      // activate the matching-index texture unit, and bind the texture to it
      //
      // DESIGN: Aren't the number of texture units limited to 16? There doesn't
      // seem to be any provision here for ensuring the size (i) of the textures
      // container for the mesh doesn't exceed 16 prior to this loop.
      //
      glActiveTexture(GL_TEXTURE0 + i);
      glBindTexture(GL_TEXTURE_2D, textures[i].id);
    }

    // draw mesh
    glBindVertexArray(vao);
    glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);

    // unbinds
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindVertexArray(0);

    // always good practice to set everything back to defaults once complete.
    glActiveTexture(GL_TEXTURE0);
  }

 private:
  /*  Mesh Data  */

  vector<Vertex> vertices{};
  vector<unsigned> indices{};
  vector<Texture> textures{};

  /*  Render data  */

  unsigned vao{}, vbo{}, ebo{};

  /*  Functions    */

  // initializes all the buffer objects/arrays
  void setupMesh()
  {
    // create buffers/arrays
    glGenVertexArrays(1, &vao);
    glGenBuffers(1, &vbo);
    glGenBuffers(1, &ebo);

    glBindVertexArray(vao);

    // load data into vertex buffers
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    // A great thing about structs is that their memory layout is sequential
    // for all its items. The effect is that we can simply pass a pointer to
    // the struct and it translates perfectly to a glm::vec3/2 array which
    // again translates to 3/2 floats which translates to a byte array.
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex),
                 &vertices[0], GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned),
                 &indices[0], GL_STATIC_DRAW);

    // set the vertex attribute pointers
    //------------------
    //

    // vertex position
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                          (void*)offsetof(Vertex, Position));
    // vertex normal
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                          (void*)offsetof(Vertex, Normal));
    // vertex texture coord
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                          (void*)offsetof(Vertex, TexCoords));
    // vertex tangent
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                          (void*)offsetof(Vertex, Tangent));
    // vertex bitangent
    glEnableVertexAttribArray(4);
    glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                          (void*)offsetof(Vertex, BiTangent));
    // unbinds
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    // ebo unbind too?
  }

};  // class Mesh
