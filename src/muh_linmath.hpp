// filename: muh_linmath.hpp
// This file is the MRS-style version of linmath.h for use with OpenGL maths

#pragma once

#include <cmath>
#include <cstdint>
#include <cstring>

using vec2 = float[2];
using vec3 = float[3];
using vec4 = float[4];
//
using mat4x4 = vec4[4];
//
using quat = float[4];

using fst_u8 = uint_fast8_t;  // looping & indexing

//------------------------------------------------------------------------------

// #define LINMATH_NO_INLINE  // uncomment to _not_ designate inlining
//
#ifdef LINMATH_NO_INLINE
#define STAT_FUNC static
#else
#define STAT_FUNC static inline
#endif

//---

// DESIGN: This macro (evil as they are) is a convenient way to allow for the
// various-sized math vector's functions to work uniformly, and to maintain
// suitable const-ness in the rest of this header, w/o writing even more code.
//
#define VEC_OPS(n)                                                       \
  STAT_FUNC void vec##n##_add(vec##n R, const vec##n a, const vec##n b)  \
  {                                                                      \
    for (fst_u8 i{0}; i < n; ++i) {                                      \
      R[i] = a[i] + b[i];                                                \
    }                                                                    \
  }                                                                      \
  STAT_FUNC void vec##n##_sub(vec##n R, const vec##n a, const vec##n b)  \
  {                                                                      \
    for (fst_u8 i{0}; i < n; ++i) {                                      \
      R[i] = a[i] - b[i];                                                \
    }                                                                    \
  }                                                                      \
  STAT_FUNC void vec##n##_scale(vec##n R, const vec##n v, const float s) \
  {                                                                      \
    for (fst_u8 i{0}; i < n; ++i) {                                      \
      R[i] = v[i] * s;                                                   \
    }                                                                    \
  }                                                                      \
  STAT_FUNC float vec##n##_mul_inner(const vec##n a, const vec##n b)     \
  {                                                                      \
    float p{};                                                           \
    for (fst_u8 i{0}; i < n; ++i) {                                      \
      p += b[i] * a[i];                                                  \
    }                                                                    \
    return p;                                                            \
  }                                                                      \
  STAT_FUNC float vec##n##_len(const vec##n v)                           \
  {                                                                      \
    return sqrtf(vec##n##_mul_inner(v, v));                              \
  }                                                                      \
  STAT_FUNC void vec##n##_norm(vec##n R, const vec##n v)                 \
  {                                                                      \
    float k{1.f / vec##n##_len(v)};                                      \
    vec##n##_scale(R, v, k);                                             \
  }                                                                      \
  STAT_FUNC void vec##n##_min(vec##n R, const vec##n a, const vec##n b)  \
  {                                                                      \
    for (fst_u8 i{0}; i < n; ++i) {                                      \
      R[i] = (a[i] < b[i]) ? a[i] : b[i];                                \
    }                                                                    \
  }                                                                      \
  STAT_FUNC void vec##n##_max(vec##n R, const vec##n a, const vec##n b)  \
  {                                                                      \
    for (fst_u8 i{0}; i < n; ++i) {                                      \
      R[i] = (a[i] > b[i]) ? a[i] : b[i];                                \
    }                                                                    \
  }
//
VEC_OPS(2)
VEC_OPS(3)
VEC_OPS(4)

//------------------------------------------------------------------------------

//
STAT_FUNC void vec3_mul_cross(vec3 R, const vec3 a, const vec3 b)
{
  R[0] = a[1] * b[2] - a[2] * b[1];
  R[1] = a[2] * b[0] - a[0] * b[2];
  R[2] = a[0] * b[1] - a[1] * b[0];
}

//------------------------------------------------------------------------------

//
STAT_FUNC void vec3_reflect(vec3 R, const vec3 v, const vec3 n)
{
  const float p{2.f * vec3_mul_inner(v, n)};

  for (fst_u8 i{0}; i < 3; ++i) {
    R[i] = v[i] - p * n[i];
  }
}

//------------------------------------------------------------------------------

//
STAT_FUNC void vec4_mul_cross(vec4 R, const vec4 a, const vec4 b)
{
  R[0] = a[1] * b[2] - a[2] * b[1];
  R[1] = a[2] * b[0] - a[0] * b[2];
  R[2] = a[0] * b[1] - a[1] * b[0];
  R[3] = 1.f;
}

//------------------------------------------------------------------------------

//
STAT_FUNC void vec4_reflect(vec4 R, const vec4 v, const vec4 n)
{
  const float p{2.f * vec4_mul_inner(v, n)};

  for (fst_u8 i{0}; i < 4; ++i) {
    R[i] = v[i] - p * n[i];
  }
}

//------------------------------------------------------------------------------

//
STAT_FUNC void mat4x4_identity(mat4x4 M)
{
  for (fst_u8 i{0}; i < 4; ++i) {
    for (fst_u8 j{0}; j < 4; ++j) {
      M[i][j] = (i == j) ? 1.f : 0.f;
    }
  }
}

//------------------------------------------------------------------------------

//
STAT_FUNC void mat4x4_dup(mat4x4 M, const mat4x4 n)
{
  for (fst_u8 i{0}; i < 4; ++i) {
    for (fst_u8 j{0}; j < 4; ++j) {
      M[i][j] = n[i][j];
    }
  }
}

//------------------------------------------------------------------------------

//
STAT_FUNC void mat4x4_row(vec4 R, const mat4x4 m, const fst_u8 i)
{
  for (fst_u8 k{0}; k < 4; ++k) {
    R[k] = m[k][i];
  }
}

//------------------------------------------------------------------------------

//
STAT_FUNC void mat4x4_col(vec4 R, const mat4x4 m, const fst_u8 i)
{
  for (fst_u8 k{0}; k < 4; ++k) {
    R[k] = m[i][k];
  }
}

//------------------------------------------------------------------------------

//
STAT_FUNC void mat4x4_transpose(mat4x4 M, const mat4x4 n)
{
  for (fst_u8 j{0}; j < 4; ++j) {
    for (fst_u8 i{0}; i < 4; ++i) {
      M[i][j] = n[j][i];
    }
  }
}

//------------------------------------------------------------------------------

//
STAT_FUNC void mat4x4_add(mat4x4 M, const mat4x4 a, const mat4x4 b)
{
  for (fst_u8 i{0}; i < 4; ++i) {
    vec4_add(M[i], a[i], b[i]);
  }
}

//------------------------------------------------------------------------------

//
STAT_FUNC void mat4x4_sub(mat4x4 M, const mat4x4 a, const mat4x4 b)
{
  for (fst_u8 i{0}; i < 4; ++i) {
    vec4_sub(M[i], a[i], b[i]);
  }
}

//------------------------------------------------------------------------------

//
STAT_FUNC void mat4x4_scale(mat4x4 M, const mat4x4 a, const float k)
{
  for (fst_u8 i{0}; i < 4; ++i) {
    vec4_scale(M[i], a[i], k);
  }
}

//------------------------------------------------------------------------------

//
STAT_FUNC void mat4x4_scale_aniso(mat4x4 M, const mat4x4 a, const float x,
                                  const float y, const float z)
{
  vec4_scale(M[0], a[0], x);
  vec4_scale(M[1], a[1], y);
  vec4_scale(M[2], a[2], z);

  for (fst_u8 i{0}; i < 4; ++i) {
    M[3][i] = a[3][i];
  }
}

//------------------------------------------------------------------------------

//
STAT_FUNC void mat4x4_mul(mat4x4 M, const mat4x4 a, const mat4x4 b)
{
  for (fst_u8 c{0}; c < 4; ++c) {
    for (fst_u8 r{0}; r < 4; ++r) {
      M[c][r] = 0.f;
      for (fst_u8 k{0}; k < 4; ++k) {
        M[c][r] += a[k][r] * b[c][k];
      }
    }
  }
}

//------------------------------------------------------------------------------

//
STAT_FUNC void mat4x4_mul_vec4(vec4 R, const mat4x4 m, const vec4 v)
{
  for (fst_u8 j{0}; j < 4; ++j) {
    R[j] = 0.f;
    for (fst_u8 i{0}; i < 4; ++i) {
      R[j] += m[i][j] * v[i];
    }
  }
}

//------------------------------------------------------------------------------

//
STAT_FUNC void mat4x4_translate(mat4x4 T, const float x, const float y,
                                const float z)
{
  mat4x4_identity(T);
  T[3][0] = x;
  T[3][1] = y;
  T[3][2] = z;
}

//------------------------------------------------------------------------------

//
STAT_FUNC void mat4x4_translate_in_place(mat4x4 M, const float x, const float y,
                                         const float z)
{
  vec4 t{x, y, z, 0.f};
  vec4 r{};

  for (fst_u8 i{0}; i < 4; ++i) {
    mat4x4_row(r, M, i);
    M[3][i] += vec4_mul_inner(r, t);
  }
}

//------------------------------------------------------------------------------

//
STAT_FUNC void mat4x4_from_vec3_mul_outer(mat4x4 M, const vec3 a, const vec3 b)
{
  for (fst_u8 i{0}; i < 4; ++i) {
    for (fst_u8 j{0}; j < 4; ++j) {
      M[i][j] = (i < 3) && (j < 3) ? (a[i] * b[j]) : 0.f;
    }
  }
}

//------------------------------------------------------------------------------

//
STAT_FUNC void mat4x4_rotate(mat4x4 R, const mat4x4 m, const float x,
                             const float y, const float z, const float angle)
{
  vec3 u{x, y, z};

  if (vec3_len(u) > 1e-4) {
    vec3_norm(u, u);

    // clang-format off
    mat4x4 s{{  0.f,  u[2], -u[1], 0.f},
             {-u[2],   0.f,  u[0], 0.f},
             { u[1], -u[0],   0.f, 0.f},
             {  0.f,   0.f,   0.f, 0.f}};
    // clang-format on
    const float sang{sinf(angle)};
    mat4x4_scale(s, s, sang);

    mat4x4 t{};
    mat4x4_from_vec3_mul_outer(t, u, u);

    mat4x4 c{};
    mat4x4_identity(c);
    mat4x4_sub(c, c, t);
    const float cang{cosf(angle)};
    mat4x4_scale(c, c, cang);

    mat4x4_add(t, t, c);
    mat4x4_add(t, t, s);

    t[3][3] = 1.f;
    mat4x4_mul(R, m, t);
  }
  else {
    mat4x4_dup(R, m);
  }
}

//------------------------------------------------------------------------------

//
STAT_FUNC void mat4x4_rotate_X(mat4x4 Q, const mat4x4 m, const float angle)
{
  const float s{sinf(angle)};
  const float c{cosf(angle)};

  // clang-format off
  const mat4x4 r{{1.f, 0.f, 0.f, 0.f},
                 {0.f,   c,   s, 0.f},
                 {0.f,  -s,   c, 0.f},
                 {0.f, 0.f, 0.f, 1.f}};
  // clang-format on

  mat4x4_mul(Q, m, r);
}

//------------------------------------------------------------------------------

//
STAT_FUNC void mat4x4_rotate_Y(mat4x4 Q, const mat4x4 m, const float angle)
{
  const float s{sinf(angle)};
  const float c{cosf(angle)};

  // clang-format off
  const mat4x4 r{{  c, 0.f,   s, 0.f},
                 {0.f, 1.f, 0.f, 0.f},
                 { -s, 0.f,   c, 0.f},
                 {0.f, 0.f, 0.f, 1.f}};
  // clang-format on

  mat4x4_mul(Q, m, r);
}

//------------------------------------------------------------------------------

//
STAT_FUNC void mat4x4_rotate_Z(mat4x4 Q, const mat4x4 m, const float angle)
{
  const float s{sinf(angle)};
  const float c{cosf(angle)};

  // clang-format off
  const mat4x4 r{{  c,   s, 0.f, 0.f},
                 { -s,   c, 0.f, 0.f},
                 {0.f, 0.f, 1.f, 0.f},
                 {0.f, 0.f, 0.f, 1.f}};
  // clang-format on

  mat4x4_mul(Q, m, r);
}

//------------------------------------------------------------------------------

//
STAT_FUNC void mat4x4_invert(mat4x4 T, const mat4x4 m)
{
  float s[6]{};
  float c[6]{};
  s[0] = m[0][0] * m[1][1] - m[1][0] * m[0][1];
  s[1] = m[0][0] * m[1][2] - m[1][0] * m[0][2];
  s[2] = m[0][0] * m[1][3] - m[1][0] * m[0][3];
  s[3] = m[0][1] * m[1][2] - m[1][1] * m[0][2];
  s[4] = m[0][1] * m[1][3] - m[1][1] * m[0][3];
  s[5] = m[0][2] * m[1][3] - m[1][2] * m[0][3];

  c[0] = m[2][0] * m[3][1] - m[3][0] * m[2][1];
  c[1] = m[2][0] * m[3][2] - m[3][0] * m[2][2];
  c[2] = m[2][0] * m[3][3] - m[3][0] * m[2][3];
  c[3] = m[2][1] * m[3][2] - m[3][1] * m[2][2];
  c[4] = m[2][1] * m[3][3] - m[3][1] * m[2][3];
  c[5] = m[2][2] * m[3][3] - m[3][2] * m[2][3];

  // Assumes it is invertible 
  const float idet{1.f / (s[0] * c[5] - s[1] * c[4] + s[2] * c[3] +
                          s[3] * c[2] - s[4] * c[1] + s[5] * c[0])};

  T[0][0] = (m[1][1] * c[5] - m[1][2] * c[4] + m[1][3] * c[3]) * idet;
  T[0][1] = (-m[0][1] * c[5] + m[0][2] * c[4] - m[0][3] * c[3]) * idet;
  T[0][2] = (m[3][1] * s[5] - m[3][2] * s[4] + m[3][3] * s[3]) * idet;
  T[0][3] = (-m[2][1] * s[5] + m[2][2] * s[4] - m[2][3] * s[3]) * idet;

  T[1][0] = (-m[1][0] * c[5] + m[1][2] * c[2] - m[1][3] * c[1]) * idet;
  T[1][1] = (m[0][0] * c[5] - m[0][2] * c[2] + m[0][3] * c[1]) * idet;
  T[1][2] = (-m[3][0] * s[5] + m[3][2] * s[2] - m[3][3] * s[1]) * idet;
  T[1][3] = (m[2][0] * s[5] - m[2][2] * s[2] + m[2][3] * s[1]) * idet;

  T[2][0] = (m[1][0] * c[4] - m[1][1] * c[2] + m[1][3] * c[0]) * idet;
  T[2][1] = (-m[0][0] * c[4] + m[0][1] * c[2] - m[0][3] * c[0]) * idet;
  T[2][2] = (m[3][0] * s[4] - m[3][1] * s[2] + m[3][3] * s[0]) * idet;
  T[2][3] = (-m[2][0] * s[4] + m[2][1] * s[2] - m[2][3] * s[0]) * idet;

  T[3][0] = (-m[1][0] * c[3] + m[1][1] * c[1] - m[1][2] * c[0]) * idet;
  T[3][1] = (m[0][0] * c[3] - m[0][1] * c[1] + m[0][2] * c[0]) * idet;
  T[3][2] = (-m[3][0] * s[3] + m[3][1] * s[1] - m[3][2] * s[0]) * idet;
  T[3][3] = (m[2][0] * s[3] - m[2][1] * s[1] + m[2][2] * s[0]) * idet;
}

//------------------------------------------------------------------------------

//
STAT_FUNC void mat4x4_orthonormalize(mat4x4 R, const mat4x4 m)
{
  mat4x4_dup(R, m);
  float s{};
  vec3 h{};

  vec3_norm(R[2], R[2]);

  s = vec3_mul_inner(R[1], R[2]);
  vec3_scale(h, R[2], s);
  vec3_sub(R[1], R[1], h);
  vec3_norm(R[1], R[1]);

  s = vec3_mul_inner(R[0], R[2]);
  vec3_scale(h, R[2], s);
  vec3_sub(R[0], R[0], h);

  s = vec3_mul_inner(R[0], R[1]);
  vec3_scale(h, R[1], s);
  vec3_sub(R[0], R[0], h);
  vec3_norm(R[0], R[0]);
}

//------------------------------------------------------------------------------

//
STAT_FUNC void mat4x4_frustum(mat4x4 M, const float l, const float r,
                              const float b, const float t, const float n,
                              const float f)
{
  M[0][0] = 2.f * n / (r - l);
  M[0][1] = M[0][2] = M[0][3] = 0.f;

  M[1][1] = 2.f * n / (t - b);
  M[1][0] = M[1][2] = M[1][3] = 0.f;

  M[2][0] = (r + l) / (r - l);
  M[2][1] = (t + b) / (t - b);
  M[2][2] = -(f + n) / (f - n);
  M[2][3] = -1.f;

  M[3][2] = -2.f * (f * n) / (f - n);
  M[3][0] = M[3][1] = M[3][3] = 0.f;
}

//------------------------------------------------------------------------------

//
STAT_FUNC void mat4x4_ortho(mat4x4 M, const float l, const float r,
                            const float b, const float t, const float n,
                            const float f)
{
  M[0][0] = 2.f / (r - l);
  M[0][1] = M[0][2] = M[0][3] = 0.f;

  M[1][1] = 2.f / (t - b);
  M[1][0] = M[1][2] = M[1][3] = 0.f;

  M[2][2] = -2.f / (f - n);
  M[2][0] = M[2][1] = M[2][3] = 0.f;

  M[3][0] = -(r + l) / (r - l);
  M[3][1] = -(t + b) / (t - b);
  M[3][2] = -(f + n) / (f - n);
  M[3][3] = 1.f;
}

//------------------------------------------------------------------------------

//
STAT_FUNC void mat4x4_perspective(mat4x4 M, const float y_fov,
                                  const float aspect, const float n,
                                  const float f)
{
  // NOTE: Degrees are unhandy units to work with, linmath uses radians for
  // everything.
  const float a{1.f / tanf(y_fov / 2.f)};

  M[0][0] = a / aspect;
  M[0][1] = 0.f;
  M[0][2] = 0.f;
  M[0][3] = 0.f;

  M[1][0] = 0.f;
  M[1][1] = a;
  M[1][2] = 0.f;
  M[1][3] = 0.f;

  M[2][0] = 0.f;
  M[2][1] = 0.f;
  M[2][2] = -((f + n) / (f - n));
  M[2][3] = -1.f;

  M[3][0] = 0.f;
  M[3][1] = 0.f;
  M[3][2] = -((2.f * f * n) / (f - n));
  M[3][3] = 0.f;
}

//------------------------------------------------------------------------------

//
STAT_FUNC void mat4x4_look_at(mat4x4 M, const vec3 eye, const vec3 center,
                              const vec3 up)
{
  // Adapted from Android's OpenGL Matrix.java.
  // See the OpenGL GLUT documentation for gluLookAt for a description
  // of the algorithm. We implement it in a straightforward way:

  // TODO: The negation of [eye] can be spared by swapping the order of
  //        operands in the following cross products in the right way.

  vec3 f{};
  vec3_sub(f, center, eye);
  vec3_norm(f, f);

  vec3 s{};
  vec3_mul_cross(s, f, up);
  vec3_norm(s, s);

  vec3 t{};
  vec3_mul_cross(t, s, f);

  M[0][0] = s[0];
  M[0][1] = t[0];
  M[0][2] = -f[0];
  M[0][3] = 0.f;

  M[1][0] = s[1];
  M[1][1] = t[1];
  M[1][2] = -f[1];
  M[1][3] = 0.f;

  M[2][0] = s[2];
  M[2][1] = t[2];
  M[2][2] = -f[2];
  M[2][3] = 0.f;

  M[3][0] = 0.f;
  M[3][1] = 0.f;
  M[3][2] = 0.f;
  M[3][3] = 1.f;

  mat4x4_translate_in_place(M, -eye[0], -eye[1], -eye[2]);
}

//------------------------------------------------------------------------------

//
STAT_FUNC void quat_identity(quat Q)
{
  Q[0] = Q[1] = Q[2] = 0.f;
  Q[3] = 1.f;
}

//------------------------------------------------------------------------------

//
STAT_FUNC void quat_add(quat R, const quat a, const quat b)
{
  for (fst_u8 i{0}; i < 4; ++i) {
    R[i] = a[i] + b[i];
  }
}

//------------------------------------------------------------------------------

//
STAT_FUNC void quat_sub(quat R, const quat a, const quat b)
{
  for (fst_u8 i{0}; i < 4; ++i) {
    R[i] = a[i] - b[i];
  }
}

//------------------------------------------------------------------------------

//
STAT_FUNC void quat_mul(quat R, const quat p, const quat q)
{
  vec3 w{};

  vec3_mul_cross(R, p, q);

  vec3_scale(w, p, q[3]);
  vec3_add(R, R, w);

  vec3_scale(w, q, p[3]);
  vec3_add(R, R, w);

  R[3] = (p[3] * q[3]) - vec3_mul_inner(p, q);
}

//------------------------------------------------------------------------------

//
STAT_FUNC void quat_scale(quat R, const quat v, const float s)
{
  for (fst_u8 i{0}; i < 4; ++i) {
    R[i] = v[i] * s;
  }
}

//------------------------------------------------------------------------------

//
STAT_FUNC float quat_inner_product(const quat a, const quat b)
{
  float p{0.f};

  for (fst_u8 i{0}; i < 4; ++i) {
    p += b[i] * a[i];
  }

  return p;
}

//------------------------------------------------------------------------------

//
STAT_FUNC void quat_conj(quat R, const quat q)
{
  for (fst_u8 i{0}; i < 3; ++i) {
    R[i] = -q[i];
  }

  R[3] = q[3];
}

//------------------------------------------------------------------------------

//
STAT_FUNC void quat_rotate(quat R, const float angle, const vec3 axis)
{
  vec3 v{};
  vec3_scale(v, axis, sinf(angle / 2.f));

  for (fst_u8 i{0}; i < 3; ++i) {
    R[i] = v[i];
  }

  R[3] = cosf(angle / 2.f);
}

//------------------------------------------------------------------------------

//
STAT_FUNC void quat_mul_vec3(vec3 R, const quat q, const vec3 v)
{
  // Method by Fabian 'ryg' Giessen (of Farbrausch):
  //   t = 2 * cross(q.xyz, v)
  //   v' = v + q.w * t + cross(q.xyz, t)

  vec3 t{};
  vec3 q_xyz{q[0], q[1], q[2]};
  vec3 u{q[0], q[1], q[2]};

  vec3_mul_cross(t, q_xyz, v);
  vec3_scale(t, t, 2);

  vec3_mul_cross(u, q_xyz, t);
  vec3_scale(t, t, q[3]);

  vec3_add(R, v, t);
  vec3_add(R, R, u);
}

//------------------------------------------------------------------------------

//
STAT_FUNC void mat4x4_from_quat(mat4x4 M, const quat q)
{
  float a{q[3]};
  float b{q[0]};
  float c{q[1]};
  float d{q[2]};
  //
  float a2{a * a};
  float b2{b * b};
  float c2{c * c};
  float d2{d * d};

  M[0][0] = a2 + b2 - c2 - d2;
  M[0][1] = 2.f * (b * c + a * d);
  M[0][2] = 2.f * (b * d - a * c);
  M[0][3] = 0.f;

  M[1][0] = 2.f * (b * c - a * d);
  M[1][1] = a2 - b2 + c2 - d2;
  M[1][2] = 2.f * (c * d + a * b);
  M[1][3] = 0.f;

  M[2][0] = 2.f * (b * d + a * c);
  M[2][1] = 2.f * (c * d - a * b);
  M[2][2] = a2 - b2 - c2 + d2;
  M[2][3] = 0.f;

  M[3][0] = M[3][1] = M[3][2] = 0.f;
  M[3][3] = 1.f;
}

//------------------------------------------------------------------------------

//
STAT_FUNC void mat4x4o_mul_quat(mat4x4 R, const mat4x4 m, const quat q)
{
  // NOTE: The way this is written only works for othogonal matrices.
  // TODO: Take care of non-orthogonal case.

  quat_mul_vec3(R[0], q, m[0]);
  quat_mul_vec3(R[1], q, m[1]);
  quat_mul_vec3(R[2], q, m[2]);

  R[3][0] = R[3][1] = R[3][2] = 0.f;
  R[3][3] = 1.f;
}

//------------------------------------------------------------------------------

//
STAT_FUNC void quat_from_mat4x4(quat Q, const mat4x4 m)
{
  float r{0.f};
  float e{};

  fst_u8 perm[]{0, 1, 2, 0, 1};
  fst_u8* p{perm};

  for (fst_u8 i{0}; i < 3; ++i) {
    e = m[i][i];
    if (e < r) continue;
    p = &perm[i];
  }

  r = sqrtf(1.f + m[p[0]][p[0]] - m[p[1]][p[1]] - m[p[2]][p[2]]);

  if (r < 1e-6) {
    Q[0] = 1.f;
    Q[1] = Q[2] = Q[3] = 0.f;
    return;
  }

  Q[0] = r / 2.f;
  Q[1] = (m[p[0]][p[1]] - m[p[1]][p[0]]) / (2.f * r);
  Q[2] = (m[p[2]][p[0]] - m[p[0]][p[2]]) / (2.f * r);
  Q[3] = (m[p[2]][p[1]] - m[p[1]][p[2]]) / (2.f * r);
}

//------------------------------------------------------------------------------

//
STAT_FUNC void mat4x4_arcball(mat4x4 R, const mat4x4 m, const vec2 _a,
                              const vec2 _b, const float s)
{
  vec2 a{};
  memcpy(a, _a, sizeof(a));
  vec2 b{};
  memcpy(b, _b, sizeof(b));

  float z_a{0.f};
  if (vec2_len(a) < 1.f)
    z_a = sqrtf(1.f - vec2_mul_inner(a, a));
  else
    vec2_norm(a, a);

  //

  float z_b{0.f};
  if (vec2_len(b) < 1.f)
    z_b = sqrtf(1.f - vec2_mul_inner(b, b));
  else
    vec2_norm(b, b);

  //

  vec3 a_{a[0], a[1], z_a};
  vec3 b_{b[0], b[1], z_b};

  vec3 c_{};
  vec3_mul_cross(c_, a_, b_);

  const float angle{acosf(vec3_mul_inner(a_, b_)) * s};
  mat4x4_rotate(R, m, c_[0], c_[1], c_[2], angle);
}

//------------------------------------------------------------------------------

/*
 * Copyright (C) 2013 Wolfgang 'datenwolf' Draxinger <code@datenwolf.net>
 *
# linmath -- A small library for linear math as required for computer graphics

linmath provides the most used types required in programming computer graphics:

- vec3 - 3 element vector of floats
- vec4 - 4 element vector of floats (4th component used for homogeneous
computations)
- mat4x4 - 4 by 4 elements matrix, computations are done in column major order
- quat - quaternion

The types are deliberately named like the types in GLSL. In fact they are meant
to be used for the client side computations and passing to same typed GLSL
uniforms.

*/
