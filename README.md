# Muh Robowaifu Simulator
## The Young Wife Series: Chii-chan Edition

### Keyboard controls:
- [w,a,s,d] or [arrow keys] - fly camera
- [key + lmb/rmb] - fly up/down
- [x] - toggle toybox display
- [m] - toggle moon display
- [z] - toggle minimal info display
- [f1] or [h] - toggle help display
- [q] - toggle wireframe mode
- [c] - toggle mouse cursor capture
- [space] - toggle simulation pause
- [bkspc] - reset scene camera
- [esc 2x] - double-tap (3s) exits program

### Program build dependencies:
- c++ (should be on your system, i'm currently using g++ 9.2)
- cmake - - https://cmake.org/
- glfw - - https://www.glfw.org/
- freetype - - https://freetype.org/
- glm - - https://glm.g-truc.net/
- assimp - asset importer library -

### Download, build, & run:
```
git clone https://gitlab.com/Chobitsu/muh-robowaifu-simulator.git
cd muh-robowaifu-simulator && mkdir build && cd build
cmake .. && make
./mrs
```
- afterwards to update to the latest, starting in the muh-robowaifu-simulator directory:
```
git pull
cd build
cmake .. && make
./mrs
```
### Recommended reading:
- learnopengl  https://learnopengl.com/
- open.gl  https://open.gl/
- glfw quickstart  https://www.glfw.org/docs/latest/quick_guide.html

### Recommended software:
- linux   (just pick one and go with it already, anon! :^)
- juci++  https://gitlab.com/cppit/jucipp

### Screenshot:
![example](wiki/capps/vert_colr_Muh Robowaifu Simulator_036.png)
