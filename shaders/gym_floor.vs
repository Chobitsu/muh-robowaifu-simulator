#version 330 core

layout (location = 0) in vec3 posna;
// use normals too?
layout (location = 2) in vec2 corda;

uniform mat4 proju;
uniform mat4 viewu;

out vec2 cord;

void main()
{
    gl_Position = proju * viewu * vec4(posna, 1.0);
    cord = corda;
}
