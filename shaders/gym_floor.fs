#version 330 core

in vec2 cord;

uniform sampler2D floor_samp;
uniform float colr_multu = 1.6;

out vec4 frag_colr;

void main()
{
  vec3 colr = texture(floor_samp, cord).rgb;
  colr *= colr_multu;

  frag_colr = vec4(colr, 1.0);
}
