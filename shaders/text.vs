#version 330 core

layout (location = 0) in vec4 vert;  // {vec2 posn, vec2 cord}

uniform mat4 proju;

out vec2 cord;

void main()
{
  gl_Position = proju * vec4(vert.xy, 0.0, 1.0);  // swizzling tbh
  cord = vert.zw;
}
