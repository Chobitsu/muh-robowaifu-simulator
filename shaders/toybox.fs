#version 330 core

in vec2 cord;
in vec3 colr;

// texture samplers
uniform sampler2D box_samp;
uniform sampler2D face_samp;

uniform float face_mixu = 0.60f;
uniform float colr_mixu;

out vec4 frag_colr;

void main()
{
	  frag_colr = mix(mix(texture(box_samp, cord),
	                      texture(face_samp, cord), face_mixu),
	                  vec4(colr, 1.0),
	                  colr_mixu);
}
