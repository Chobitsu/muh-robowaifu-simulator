#version 330 core

layout (location = 0) in vec3 posna;
layout (location = 1) in vec3 colra;
layout (location = 2) in vec2 corda;

uniform mat4 modlu;
uniform mat4 viewu;
uniform mat4 proju;

out vec3 colr;
out vec2 cord;

void main()
{
	gl_Position = proju * viewu * modlu * vec4(posna, 1.0f);
	colr = colra;
	cord = corda;
}
