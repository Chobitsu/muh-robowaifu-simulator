#version 330 core

in vec2 cord;

uniform sampler2D text_samp;
uniform vec3 text_colru;

out vec4 frag_colr;

void main()
{
    vec4 transp = vec4(1.0, 1.0, 1.0, texture(text_samp, cord).r);
    frag_colr = vec4(text_colru, 1.0) * transp;
}
